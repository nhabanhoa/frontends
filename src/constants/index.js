export const API_METHOD = {
  SERVER_URL: 'http://localhost:8000/',
  METHOD_GET: 'GET',
  METHOD_POST: 'POST',
  METHOD_PUT: 'PUT',
  METHOD_DELETE: 'DELETE'
}

export const PAGE_TYPE = {
  AUTH: 'AUTH',
  PRIVATE: 'PRIVATE',
  PUBLIC: 'PUBLIC',
  ADMIN: 'ADMIN'
}

export const ACTIONS ={
  LOADING: 'LOADING',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
}

export const PATTERN = {
  PASSWORD: new RegExp('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$'),
  PHONE: new RegExp('^0[0-9]{9}'),
  EMAIL: new RegExp('\\b[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b')
}

export const PROPERTY = {
  COLOR: "COLOR",
  ORIGIN: "ORIGIN",
  PACKING: "PACKING",
  SELLBLOCK: "SELLBLOCK",
  TYPE: "TYPE"
}
import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

//i18n
import { withTranslation } from "react-i18next"
import AdminSidebarContent from "./AdminSidebarContent"

const Sidebar = props => {

  // const getSidebarContent = () => {
  //   if(props.branch.includes("admin"))
  //     return <AdminSidebarContent/>
  //   if(props.branch.includes("manager"))
  //     return <ManagerSidebarContent/>
  //   if(props.branch.includes("accountant"))
  //     return <AccoutantSidebarContent/>
  // }

  return (
    <React.Fragment>
      <div className="vertical-menu">
        <div data-simplebar className="h-100">
          {
            // getSidebarContent()
            props.type !== "condensed" ? <AdminSidebarContent /> : <AdminSidebarContent />
          }
        </div>
      </div>
    </React.Fragment>
  )
}

Sidebar.propTypes = {
  type: PropTypes.string,
  branch: PropTypes.string
}

const mapStatetoProps = state => {
  return {
    layout: state.Layout,
  }
}
export default connect(
  mapStatetoProps,
  {}
)(withRouter(withTranslation()(Sidebar)))

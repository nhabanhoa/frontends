import PropTypes from "prop-types"
import React, { useEffect, useRef } from "react"

// //Import Scrollbar
import SimpleBar from "simplebar-react"

// MetisMenu
import MetisMenu from "metismenujs"
import { Link, withRouter } from "react-router-dom"

//i18n
import { withTranslation } from "react-i18next"
import logoDarkSm from "_assets/images/logos/logo-no-name.png";
import logoDarkLg from "_assets/images/logos/logo.png";
import logoLightSm from "_assets/images/logos/logo-no-name-light.png";
import logoLightLg from "_assets/images/logos/logo-light.png";

const AdminSidebarContent = props => {
  const ref = useRef()
  // Use ComponentDidMount and ComponentDidUpdate method symultaniously
  useEffect(() => {
    const pathName = props.location.pathname

    const initMenu = () => {
      new MetisMenu("#side-menu")
      let matchingMenuItem = null
      const ul = document.getElementById("side-menu")
      const items = ul.getElementsByTagName("a")
      for (let i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i]
          break
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem)
      }
    }
    initMenu()
  }, [window.location.pathname])

  useEffect(() => {
    ref.current.recalculate()
  })

  function scrollElement(item) {
      if (item) {
          const currentPosition = item.offsetTop
          if (currentPosition > window.innerHeight) {
            ref.current.getScrollElement().scrollTop = currentPosition - 300
          }
      }
  }   

  function activateParentDropdown(item) {
    item.classList.add("active")
    const parent = item.parentElement
    const parent2El = parent.childNodes[1]
    if (parent2El && parent2El.id !== "side-menu") {
      parent2El.classList.add("mm-show")
    }

    if (parent) {
      parent.classList.add("mm-active")
      const parent2 = parent.parentElement

      if (parent2) {
        parent2.classList.add("mm-show") // ul tag

        const parent3 = parent2.parentElement // li tag

        if (parent3) {
          parent3.classList.add("mm-active") // li
          parent3.childNodes[0].classList.add("mm-active") //a
          const parent4 = parent3.parentElement // ul
          if (parent4) {
            parent4.classList.add("mm-show") // ul
            const parent5 = parent4.parentElement
            if (parent5) {
              parent5.classList.add("mm-show") // li
              parent5.childNodes[0].classList.add("mm-active") // a tag
            }
          }
        }
      }
      scrollElement(item);
      return false
    }
    scrollElement(item);
    return false
  }

  return (
    <React.Fragment>
      <SimpleBar className="vertical-menu" style={{ maxHeight: "100%" }} ref={ref}>
        <div className="navbar-brand-box">
          <Link to="/" className="logo logo-dark">
                <span className="logo-sm">
                  <img src={logoDarkSm} alt="" height="28" />
                </span>
            <span className="logo-lg">
                  <img src={logoDarkLg} alt="" height="32" />
                </span>
          </Link>

          <Link to="/" className="logo logo-light">
                <span className="logo-sm">
                  <img src={logoLightSm} alt="" height="28" />
                </span>
            <span className="logo-lg">
                  <img src={logoLightLg} alt="" height="32" />
                </span>
          </Link>
        </div>
        <div id="sidebar-menu">
          <ul className="metismenu list-unstyled" id="side-menu">

            <li>
              <Link to="/#" className="has-arrow waves-effect">
                <i className="bx bx-package"/>
                <span>{props.t("adminDashboard.import-order")}</span>
              </Link>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <Link to="/admin/create-import-order">
                    {props.t("common.addNew")}
                  </Link>
                </li>
                <li>
                  <Link to="/admin/list-import-order">
                    {props.t("common.list")}
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/#" className="has-arrow waves-effect">
                <i className="bx bx-cart"/>
                <span>{props.t("adminDashboard.export-order")}</span>
              </Link>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <Link to="/admin/create-export-order">
                    {props.t("common.addNew")}
                  </Link>
                </li>
                <li>
                  <Link to="/admin/list-export-order">
                    {props.t("common.list")}
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/#" className="has-arrow waves-effect">
                <i className="bx bx-box"/>
                <span>{props.t("adminDashboard.product")}</span>
              </Link>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <Link to="/admin/create-product">
                    {props.t("common.addNew")}
                  </Link>
                </li>
                <li>
                  <Link to="/admin/list-product">
                    {props.t("common.list")}
                  </Link>
                </li>
                <li>
                  <Link to="/admin/product-properties">
                    {props.t("product.properties")}
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/#" className="has-arrow waves-effect">
                <i className="bx bx-images"/>
                <span>{props.t("adminDashboard.images")}</span>
              </Link>
              <ul className="sub-menu" aria-expanded="false">
                <li>
                  <Link to="/admin/list-images">
                    {props.t("common.list")}
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </SimpleBar>
    </React.Fragment>
  )
}

AdminSidebarContent.propTypes = {
  location: PropTypes.object,
  t: PropTypes.any,
}

export default withRouter(withTranslation()(AdminSidebarContent))

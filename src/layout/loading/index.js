import React from 'react'

import './style.scss'

const Loading = props => {
  return (
    <div className="app-loading">
      <div className="loading-content">
        <span/>
        <span/>
        <span/>
        <span/>
        <span/>
      </div>
    </div>
  )
}

export default Loading
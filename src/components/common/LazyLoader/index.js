import React from 'react'

const LazyLoader = () => (
    <div className="loading">
        <h5>Loading...</h5>
    </div>
)

export default LazyLoader
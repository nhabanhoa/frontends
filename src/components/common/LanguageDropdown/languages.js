import viFlag from "_assets/images/flags/vietnam.png"
import ukFlag from "_assets/images/flags/united-kingdom.png"


const languages = {
  vi: {
    label: "Tiếng Việt",
    flag: viFlag
  },
  en: {
    label: "English",
    flag: ukFlag,
  }
}

export default languages

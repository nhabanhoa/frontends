import React from 'react'

const NotFound = () => {
  return (
    <div style={{
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      flexDirection: 'column',
      alignItems: 'center'
    }}>
      <i
        className="fas fa-box-open"
        style={{fontSize: '50px', marginTop: '5%'}}
      />
      <span className="mt-1 mb-2 font-weight-semibold">No Data</span>
    </div>
  )
}

export default NotFound
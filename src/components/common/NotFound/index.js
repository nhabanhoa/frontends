import React from "react";
import loadable from "_utils/loadable"

export default loadable(() => import("./NotFound"), {
    fallback: <div>...</div>
})
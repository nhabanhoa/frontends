'use strict'

import React from 'react'
import PropTypes from 'prop-types'
import {SwatchesPicker} from 'react-color'

class ColorPicker extends React.Component {
  state = {
    displayColorPicker: false,
  };

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChange = (color) => {
    this.setState({ color: color.rgb })
  };

  render() {
    return (
      <div>
        <div style={{
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        }} onClick={ this.handleClick }>
          <div style={{
            width: '187.6px',
            height: '36.23px',
            borderRadius: '2px',
            background: `${this.props.color}`,
          }}  />
        </div>
        { this.state.displayColorPicker ? <div style={{
          position: 'absolute',
          zIndex: '2',
        }}>
          <div style={{
                  position: 'fixed',
                  top: '0px',
                  right: '0px',
                  bottom: '0px',
                  left: '0px',
                }} onClick={ this.handleClose }/>
          <SwatchesPicker
            color={ this.props.color }
            onChange={color => {
              this.props.setColorCode(color.hex)
            }}/>
        </div> : null }

      </div>
    )
  }
}

export default ColorPicker

ColorPicker.propTypes = {
  color: PropTypes.string,
  setColorCode: PropTypes.func
}
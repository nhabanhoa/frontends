import React from 'react'
import {Card, CardBody } from "reactstrap"
import { Link } from 'react-router-dom'

const ProductCard = ({product}) => {
    return (
      <Link>
        <Card>
          <CardBody>
            <div className="product-img position-relative">
              <img
                src={product.images[0].s3Path}
                alt={product.displayName}
                className="img-fluid mx-auto d-block product-img-sm"
              />
            </div>
            <div className="mt-4 text-center">
              <h5 className="mb-3 text-truncate">{product.displayName} </h5>
            </div>
            <div className="mt-4 text-center">
              <h5 className="mb-3 text-truncate">{product.displayName} </h5>
            </div>
          </CardBody>
        </Card>
      </Link>
    );
}

export default ProductCard
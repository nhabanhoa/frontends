import React from "react"
import {toast} from 'react-toastify'

export function success(nodeOrMsg) {
  return toast.success(
    <div>
      {nodeOrMsg}
    </div>
  );
}

export function error(nodeOrMsg) {
  return toast.error(
    <div>
      {nodeOrMsg}
    </div>
  );
}

export function info(nodeOrMsg) {
  return toast.info(
    <div>
      {nodeOrMsg}
    </div>
  );
}

export function warn(nodeOrMsg) {
  return toast.warn(
    <div>
      {nodeOrMsg}
    </div>
  );
}
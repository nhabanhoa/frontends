import {
  S3Client,
  PutObjectCommand,
  ListObjectsCommand,
  DeleteObjectCommand,
  DeleteObjectsCommand
} from "@aws-sdk/client-s3"

const s3 = new S3Client({
  region: process.env.AWS_REGION,
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  }
})

export const uploadFilesForProductS3 = async (files) => {
  // console.log("upload files", files)
  let file = files[0]
  // console.log("upload file", file)
  let fileName = file.name
  // console.log("upload file.name", file.name)
  let photoKey = process.env.AWS_S3_BUCKET_KEY_FOR_PRODUCT + '/' + fileName + "_" + new Date().getTime()
  
  let uploadParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME_PIC,
    Key: photoKey,
    Body: file
  }
  try{
    const data = await s3.send(new PutObjectCommand(uploadParams))
    console.log("upload data s3", data)
    return photoKey
  } catch (error) {
    // console.log("error upload image", error)
    return error
  }
}

export const uploadFilesForUserS3 = async (files) => {
  // console.log("upload files", files)
  let file = files[0]
  // console.log("upload file", file)
  let fileName = file.name
  // console.log("upload file.name", file.name)
  let photoKey = process.env.AWS_S3_BUCKET_KEY_FOR_USER + '/' + fileName + "_" + new Date().getTime()
  
  let uploadParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME_PIC,
    Key: photoKey,
    Body: file
  }
  try{
    const data = await s3.send(new PutObjectCommand(uploadParams))
    console.log("upload data s3", data)
    return photoKey
  } catch (error) {
    // console.log("error upload image", error)
    return error
  }
}






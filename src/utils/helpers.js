//authentication
export const checkAuth = () => {
  return false;
}

export const checkAdmin = () => {
  return true;
}

export const isEmptyArray = arr => (arr === null || arr.length === 0)
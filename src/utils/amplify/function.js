import {Auth} from "@aws-amplify/auth"

export const amplifySignUp = async (userInfo) => {
  try {
    const { user } = await Auth.signUp({
      username: userInfo.username.replace(/0/i, '+84'),
      password: userInfo.password,
      attributes: {
        email: userInfo.email
      }
    })

    return user
  } catch (error) {
    // console.log("Error signup: ", error)
    throw error
  }
}
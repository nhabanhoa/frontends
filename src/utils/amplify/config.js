import Amplify from '@aws-amplify/core';
import awsconfig from '_src/aws-exports';
Amplify.configure(awsconfig);
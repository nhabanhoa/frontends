import PropTypes from 'prop-types'
import React, { useEffect } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

import { logoutUser } from "_store/actions"

const LogoutPage = props => {
  useEffect(() => {
    props.logoutUser(props.history)
  })

  return <></>
}

LogoutPage.propTypes = {
  history: PropTypes.object,
  logoutUser: PropTypes.func
}

export default withRouter(connect(null, { logoutUser })(LogoutPage))
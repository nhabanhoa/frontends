import PropTypes from 'prop-types'
import MetaTags from 'react-meta-tags';
import React from "react"
import { Row, Col, Alert, Card, CardBody, Container } from "reactstrap"

// Redux
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// action
import { userForgetPassword } from "_store/actions"

// import images
import profile from "_assets/images/profile-img.png"
import logo from "_assets/images/logos/logo-no-name.png"

//i18next
import {withTranslation} from "react-i18next";

const ForgetPasswordPage = props => {
  function handleValidSubmit(event, values) {
    props.userForgetPassword(values, props.history)
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>{props.t("forgotPasswordPage.title")}</title>
      </MetaTags>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="bx bxs-home-circle h2" />
        </Link>
      </div>
      <div className="account-pages pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-primary bg-soft bg-soft-primary">
                  <Row>
                    <Col xs={7}>
                      <div className="text-primary p-4">
                        <h5 className="text-primary">{props.t("forgotPasswordPage.form.title")}</h5>
                        <p>{props.t("forgotPasswordPage.form.hint")}</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profile} alt="" className="img-fluid" />
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    {props.forgetError && props.forgetError ? (
                      <Alert color="danger" style={{ marginTop: "13px" }}>
                        {props.forgetError}
                      </Alert>
                    ) : null}
                    {props.forgetSuccessMsg ? (
                      <Alert color="success" style={{ marginTop: "13px" }}>
                        {props.forgetSuccessMsg}
                      </Alert>
                    ) : null}

                    <AvForm
                      className="form-horizontal"
                      onValidSubmit={(e, v) => handleValidSubmit(e, v)}
                    >
                      <div className="mb-3">
                        <AvField
                          name="email"
                          label={props.t("forgotPasswordPage.form.emailField.label")}
                          className="form-control"
                          placeholder={props.t("forgotPasswordPage.form.emailField.placeholder")}
                          required
                          validate={{
                            email: {value: true, errorMessage: 'Invalid email address'},
                            required: {value: true, errorMessage: 'Please enter your email'},
                            minLength: {value: 6, errorMessage: 'Your email must be between 6 and 50 characters'},
                            maxLength: {value: 50, errorMessage: 'Your email must be between 6 and 50 characters'}
                          }}
                        />
                      </div>
                      <Row className="mb-3">
                        <Col className="text-right">
                          <button
                            className="btn btn-primary w-md waves-effect waves-light"
                            type="submit"
                          >
                            {props.t("forgotPasswordPage.form.buttonReset")}
                          </button>
                        </Col>
                      </Row>
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  {props.t("forgotPasswordPage.goBack")}{" "}
                  <Link to="login" className="font-weight-medium text-primary">
                    {props.t("loginPage.title")}
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} {props.t("Footer-claim-craft-with")}{" "}
                  <i className="bx bx-heart text-danger" /> {props.t("by-NPMT")}
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

ForgetPasswordPage.propTypes = {
  forgetError: PropTypes.any,
  forgetSuccessMsg: PropTypes.any,
  history: PropTypes.object,
  userForgetPassword: PropTypes.func
}

const mapStatetoProps = state => {
  const { forgetError, forgetSuccessMsg } = state.ForgetPassword
  return { forgetError, forgetSuccessMsg }
}

export default withRouter(
  connect(mapStatetoProps, { userForgetPassword })
    (withTranslation()(ForgetPasswordPage))
)

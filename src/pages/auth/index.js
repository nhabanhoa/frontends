//Route
import AuthRoute from '_router/AuthRoute'

//Layout
import PublicLayout from "_layout/public";

//Pages
import LoginPage from '_pages/auth/LoginPage'
import RegisterPage from "_pages/auth/RegisterPage";
import ForgetPasswordPage from "_pages/auth/ForgetPasswordPage";

const routeConfig = {
  layout: PublicLayout,
  route: AuthRoute
}

const AuthRoutes = [
  {
    ...routeConfig,
    title: "Login Page",
    component: LoginPage,
    path: '/login',
  },
  {
    ...routeConfig,
    title: "Register Page",
    component: RegisterPage,
    path: "/register"
  },
  {
    ...routeConfig,
    title: "Forget Password Page",
    component: ForgetPasswordPage,
    path: "/forgot-password"
  }
]

export default AuthRoutes
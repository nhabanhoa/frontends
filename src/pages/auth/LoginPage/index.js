import PropTypes from 'prop-types'
import MetaTags from 'react-meta-tags';
import React from "react"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

// Redux
import { connect } from "react-redux"
import { withRouter, Link } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

//i18next
import { withTranslation } from "react-i18next"

// actions
import { loginUser, apiError } from "_store/actions"

// import images
import profile from "_assets/images/profile-img.png"
import logo from "_assets/images/logos/logo-no-name.png"

const LoginPage = props => {
  // handleValidSubmit
  const handleValidSubmit = (event, values) => {
    props.loginUser(values, props.history)
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>{props.t("login.title")}</title>
      </MetaTags>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="bx bxs-home-circle h2" />
        </Link>
      </div>
      <div className="account-pages pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-primary bg-soft">
                  <Row>
                    <Col xs={7}>
                      <div className="text-primary p-4">
                        <h5 className="text-primary">{props.t("welcome-back")}</h5>
                        <p>{props.t("signin-to")}</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profile} alt="" className="img-fluid" />
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/" className="auth-logo-light">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    <AvForm
                      className="form-horizontal"
                      onValidSubmit={(e, v) => {
                        handleValidSubmit(e, v)
                      }}
                    >
                      {props.error && typeof props.error === "string" ? (
                        <Alert color="danger">{props.error}</Alert>
                      ) : null}

                      <div className="mb-3">
                        <AvField
                          name="telephone"
                          label={props.t("telephone")}
                          className="form-control"
                          placeholder={props.t("input-telephone")}
                          validate={{
                            pattern: {value: '/^0[0-9]{9}/g', errorMessage: 'Phone number is invalid!!!'}
                          }}

                          required
                        />
                      </div>

                      <div className="mb-3">
                        <AvField
                          name="password"
                          label={props.t("password")}
                          type="password"
                          validate={{
                            pattern: {
                              value: '/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{0,}$/g',
                              errorMessage: 'Password need to contains at least one uppercase letter, one lowercase letter, one number and one special character'
                            },
                            minLength: {value: 6, errorMessage: 'Password length need to be at lest 6 characters'}
                          }}
                          required
                          placeholder={props.t("input-password")}
                        />
                      </div>

                      <div className="form-check">
                        <input
                          type="checkbox"
                          className="form-check-input"
                          id="customControlInline"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customControlInline"
                        >
                          {props.t("remember-me")}
                        </label>
                      </div>

                      <div className="mt-3 d-grid">
                        <button
                          className="btn btn-primary btn-block waves-effect waves-light"
                          type="submit"
                        >
                          {props.t("login")}
                        </button>
                      </div>

                      <div className="mt-4 text-center">
                        <Link to="/forgot-password" className="text-muted">
                          <i className="bx bx-lock me-1" />
                          {props.t("forgot-password")}?
                        </Link>
                      </div>
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  {props.t("dont-have-account")} ?{" "}
                  <Link
                    to="register"
                    className="fw-medium text-primary"
                  >
                    {" "}
                    {props.t("signup-now")}{" "}
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} {props.t("Footer-claim-craft-with")}{" "}
                  <i className="bx bx-heart text-danger" /> {props.t("by-NPMT")}
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const { error } = state.Login
  return { error }
}

export default withRouter(
  connect(mapStateToProps, { loginUser, apiError })(withTranslation()(LoginPage))
)

LoginPage.propTypes = {
  error: PropTypes.any,
  history: PropTypes.object,
  loginUser: PropTypes.func
}
import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"
import MetaTags from 'react-meta-tags';
import { Row, Col, CardBody, Card, Alert, Container, Button } from "reactstrap"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// action
import { registerUser } from "_store/actions"

// Redux
import { connect } from "react-redux"
import { Link } from "react-router-dom"

// import images
import profileImg from "_assets/images/profile-img.png"
import logoImg from "_assets/images/logos/logo-no-name.png"
import {withTranslation} from "react-i18next";

// forms
import CustomerInfoForm from "_pages/auth/RegisterPage/CustomerInfoForm";
import CredentialInfoForm from "_pages/auth/RegisterPage/CredentialInfoForm";

const RegisterPage = props => {
  const [activeStep, setActiveStep] = useState("1")
  const [phone, setPhone] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [storeName, setStoreName] = useState("")
  const [storeAddress, setStoreAddress] = useState("")
  const [formCredentialValid, setFormCredentialValid] = useState(false)

  const renderForm = () => {
    if (activeStep === "1") {
      return <CredentialInfoForm
        setActiveStep={setActiveStep}
        t={props.t}
        phone={phone}
        setPhone={setPhone}
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        confirmPassword={confirmPassword}
        setConfirmPassword={setConfirmPassword}
        formCredentialValid={formCredentialValid}
        setFormCredentialValid={setFormCredentialValid}
      />
    }
    if (activeStep === "2") {
      return <CustomerInfoForm
        setActiveStep={setActiveStep}
        t={props.t}
        storeName={storeName}
        setStoreName={setStoreName}
        storeAddress={storeAddress}
        setStoreAddress={setStoreAddress}
        handleValidSubmit={handleValidSubmit}
        formCredentialValid={formCredentialValid}
        loading={props.loading}
      />
    }
  }

  // handleValidSubmit
  const handleValidSubmit = () => {
    // console.log("submit 1")
    props.registerUser({
      username: phone,
      password: password,
      attributes: {
        email: email
      }
    })
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>{props.t("register")}</title>
      </MetaTags>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="bx bxs-home-circle h2" />
        </Link>
      </div>
      <div className="account-pages pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col xs={12} md={8} lg={8} xl={8}>
              <Card className="overflow-hidden">
                <div className="bg-primary bg-soft">
                  <Row>
                    <Col className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">{props.t("free-register")}</h5>
                        <p>{props.t("create-free-account")}</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profileImg} alt="" className="img-fluid" />
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logoImg}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    <AvForm
                      className="form-horizontal"
                      onSubmit={(e, v) => {
                        // console.log("submit")
                        handleValidSubmit()
                      }}
                    >
                      {props.user && props.user ? (
                        <Alert color="success">
                          Register User Successfully
                        </Alert>
                      ) : null}

                      {props.registrationError &&
                      props.registrationError ? (
                        <Alert color="danger">
                          {props.registrationError}
                        </Alert>
                      ) : null}

                      {
                        renderForm()
                      }

                      <div className="mt-4 text-center">
                        <p className="mb-0">
                          By registering you agree to the Skote{" "}
                          <Link to="#" className="text-primary">
                            Terms of Use
                          </Link>
                        </p>
                      </div>
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  Already have an account ?{" "}
                  <Link to="/login" className="font-weight-medium text-primary">
                    {" "}
                    Login
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} Skote. Crafted with{" "}
                  <i className="bx bx-heart text-danger" /> by Themesbrand
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

RegisterPage.propTypes = {
  registerUser: PropTypes.func,
  user: PropTypes.any,
  t: PropTypes.any
}

const mapStateToProps = state => {
  const { user, registrationError, loading } = state.Account
  return { user, registrationError, loading }
}

export default connect(mapStateToProps, {
  registerUser,
})(withTranslation()(RegisterPage))

import React, {useEffect, useState} from "react"
import {Row, Col, Button} from "reactstrap"

import {AvField} from "availity-reactstrap-validation"

const CustomerInfoForm = props => {

  const [formInfoValid, setFormInfoValid] = useState(false)

  useEffect(() => {
    // console.log("storeName", !props.storeName)
    // console.log("storeAddress", !props.storeAddress)

    setFormInfoValid(props.storeName && props.storeAddress)
  }, [props.storeAddress, props.storeName])

  return <>
    <Row>
      <Col xs={12} md={6}>
        <div className="mb-3">
          <AvField
            name="shopname"
            label={props.t("shopname")}
            required
            placeholder={props.t("input-shopname")}
            value={props.storeName}
            onChange={(ctx, values) => props.setStoreName(values)}
          />
        </div>
      </Col>

      <Col xs={12} md={6}>
        <div className="mb-3">
          <AvField
            name="shopaddress"
            label={props.t("shopaddress")}
            required
            placeholder={props.t("input-shopaddress")}
            value={props.storeAddress}
            onChange={(ctx, values) => props.setStoreAddress(values)}
          />
        </div>
      </Col>
    </Row>

    <div className="mt-4 ms-4 me-4 space-between-child">
      <Button
        className="btn btn-primary btn-block waves-effect waves-light"
        color="primary"
        size="lg"
        onClick={() => {
          props.setActiveStep("1")
        }}
      >
        Back
      </Button>

      <Button
        className="btn btn-primary btn-block waves-effect waves-light"
        color={(props.formCredentialValid && formInfoValid) ? "primary" : "secondary"}
        size="lg"
        type="submit"
        // onClick={() => props.handleValidSubmit}
        disabled={!(props.formCredentialValid && formInfoValid) && !props.loading}
      >
        Register
      </Button>
    </div>
  </>
}

export default CustomerInfoForm
import React, {useEffect, useState} from "react"
import {Row, Col, Button} from "reactstrap"

import {AvField, AvForm} from "availity-reactstrap-validation"
import {PATTERN} from "_constants";

const CredentialInfoForm = props => {

  useEffect(() => {
    props.setFormCredentialValid(
      checkPhoneValidate()&&
      checkPasswordValidate()&&
      checkEmailValidate()&&
      checkConfirmPasswordValidate()
    )
  },[
    props.phone,
    props.email,
    props.password,
    props.confirmPassword
  ])

  const checkPhoneValidate = () =>
    props.phone.match(PATTERN.PHONE) !== null

  const checkEmailValidate = () =>
    props.email.match(PATTERN.EMAIL) !== null

  const checkPasswordValidate = () =>
    props.password.match(PATTERN.PASSWORD) !== null

  const checkConfirmPasswordValidate = () =>
    props.confirmPassword === props.password

  const validateConfirmPassword = (value, ctx) => {
    if (ctx.password === value)
    {
      return true
    }
    else
    {
      return false
    }
  }

  return <>
    <Row>
    <Col xs={12} md={6}>
      <div className="mb-3">
        <AvField
          name="telephone"
          label={props.t("telephone")}
          className="form-control"
          placeholder={props.t("input-telephone")}
          validate={{
            pattern: {value: PATTERN.PHONE, errorMessage: 'Phone number is invalid!!!'}
          }}
          value={props.phone}
          onChange={(ctx, values) => props.setPhone(values)}
          required
        />
      </div>

      <div className="mt-3">
        <AvField
          name="password"
          label={props.t("password")}
          type="password"
          validate={{
            pattern: {
              value: PATTERN.PASSWORD,
              errorMessage: 'Password need to contains at least one uppercase letter, one lowercase letter, one number and one special character'
            },
            minLength: {value: 6, errorMessage: 'Password length need to be at lest 6 characters'}
          }}
          value={props.password}
          onChange={(ctx, values) => props.setPassword(values)}
          required
          placeholder={props.t("input-password")}
        />
      </div>
    </Col>

    <Col xs={12} md={6}>
      <div className="mb-3">
        <AvField
          name="email"
          label="Email"
          required
          placeholder={props.t("input-email")}
          validate={{
            pattern: {
              value: PATTERN.EMAIL,
              errorMessage: props.t("email-pattern-error")
            }
          }}
          value={props.email}
          onChange={(ctx, values) => props.setEmail(values)}
        />
      </div>

      <div className="mt-3">
        <AvField
          name="confirmPassword"
          label="Confirm Password"
          type="password"
          required
          placeholder="Enter Confirm Password"
          validate={{
            myValidation: validateConfirmPassword,
          }}
          value={props.confirmPassword}
          onChange={(ctx, values) => props.setConfirmPassword(values)}
        />
      </div>
    </Col>
  </Row>

    <div className="mt-4 me-4 right-child">
      <Button
        className="btn btn-primary btn-block waves-effect waves-light"
        color={props.formCredentialValid ? "primary" : "secondary"}
        disabled={!props.formCredentialValid}
        size="lg"
        type="submit"
        onClick={() => {
          props.setActiveStep("2")
        }}
      >
        Next
      </Button>
    </div>
  </>
}

export default CredentialInfoForm
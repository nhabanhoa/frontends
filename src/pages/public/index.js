import PublicRoute from '_router/PublicRoute'

//Layout
import PublicLayout from "_layout/public";

// import HomePage from './HomePage'
const routeConfig = {
  layout: PublicLayout,
  route: PublicRoute
}

const PublicRoutes = [
  // {
  //   ...routeConfig,
  //   title: "Home Page",
  //   component: HomePage,
  //   path: '/'
  // }
]

export default PublicRoutes
import React, {useState, useEffect, Suspense} from 'react'
import { lazy } from "react";

import API from '_api'
// packages
import { useHistory } from 'react-router-dom'

//styles
import HomeStyles from "./homeStyles";

// components

import IntroContent from "./content/IntroContent.json";
import MiddleBlockContent from "./content/MiddleBlockContent.json";
import AboutContent from "./content/AboutContent.json";
import MissionContent from "./content/MissionContent.json";
import ProductContent from "./content/ProductContent.json";
import ContactContent from "./content/ContactContent.json";
import Header from "_components/homepage/Header";
import Footer from "_components/homepage/Footer";

const ContactFrom = lazy(() => import("_components/homepage/ContactForm"));
const ContentBlock = lazy(() => import("_components/homepage/ContentBlock"));
const MiddleBlock = lazy(() => import("_components/homepage/MiddleBlock"));
const Container = lazy(() => import("_components/common/Container"));
const ScrollToTop = lazy(() => import("_components/common/ScrollToTop"));
const HomePage = () => {
  return (
    <div>
      <HomeStyles />
      <Header/>
        <Container>
            <ScrollToTop />
            <ContentBlock
              type="right"
              first="true"
              title={IntroContent.title}
              content={IntroContent.text}
              button={IntroContent.button}
              icon="developer.svg"
              id="intro"
            />
            <MiddleBlock
              title={MiddleBlockContent.title}
              content={MiddleBlockContent.text}
              button={MiddleBlockContent.button}
            />
            <ContentBlock
              type="left"
              title={AboutContent.title}
              content={AboutContent.text}
              section={AboutContent.section}
              icon="graphs.svg"
              id="about"
            />
            <ContentBlock
              type="right"
              title={MissionContent.title}
              content={MissionContent.text}
              icon="product-launch.svg"
              id="mission"
            />

            <ContentBlock
              type="left"
              title={ProductContent.title}
              content={ProductContent.text}
              icon="waving.svg"
              id="product"
            />
            <ContactFrom
              title={ContactContent.title}
              content={ContactContent.text}
              id="contact"
            />
        </Container>
        <Footer/>
    </div>

  );
}

export default HomePage
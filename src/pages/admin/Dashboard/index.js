import PropTypes from 'prop-types'
import MetaTags from 'react-meta-tags';
import React from "react"
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardBody,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  Table,
} from "reactstrap"

// Redux
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"

//i18next
import { withTranslation } from "react-i18next"

const AdminDashboard = props => {
  return (
    <React.Fragment>
      <MetaTags>
        <title>Dashboard</title>
      </MetaTags>
      <div className="account-pages pt-sm-5">
        <Container>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  return {}
}

export default withRouter(
  connect(mapStateToProps, {})(withTranslation()(AdminDashboard))
)

AdminDashboard.propTypes = {
}
import React, {lazy, useState} from 'react'
import PropTypes from "prop-types";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"

import NotFound from "_components/common/NotFound";

import {
  createPropertyPacking,
  deletePropertyPacking,
  updatePropertyPacking
} from "_store/properties/packing/actions";

const ReactTable = lazy(() => import('react-table-v6'))

const PackingPropertyTab = props => {
  const [addPackingModal, setAddPackingModal] = useState(false)

  const [packingNumber, setPackingNumber] = useState(0)
  const [packingUnit, setPackingUnit] = useState("")
  const [packingStyle, setPackingStyle] = useState("")

  const columns = [
    {
      id: "id",
      Header: "ID",
      accessor: "id",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "number-unit",
      accessor: packing => <div>{`${packing.number} ${packing.unit.toLocaleLowerCase('vi-VN')}/${packing.style.toLocaleLowerCase('vi-VN')}`}</div>,
      Header: "Packing Style",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "action",
      accessor: type => <button
        className="btn btn-danger"
        onClick={() => {
          props.deletePropertyPacking(type.id)
        }}
      >Delete</button>,
      Header: "Action",
      headerClassName: "text-center",
      className: "text-center"
    }
  ]

  return (
    <Card>
      <CardBody className="pt-0">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <CardTitle>Packings</CardTitle>
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target=".modal-center"
            onClick={() => setAddPackingModal(true)}
          >
            <i className="bx bx-plus"/>
            Add Packings
          </button>
        </div>

        <Modal
          className="modal-center"
          isOpen={addPackingModal}
          toggle={() => setAddPackingModal(!addPackingModal)}
          centered={true}
        >
          <div className="modal-header">
            <h5 className="modal-title mt-0">Add Packing</h5>
            <button
              type="button"
              onClick={() => {
                setAddPackingModal(false)
              }}
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <div className="mb-3 me-2">
                <Label>Packing Number</Label>
                <Input
                  id="packingnumber"
                  name="packingnumber"
                  type="number"
                  onChange={e => {
                    setPackingNumber(parseInt(e.target.value))
                  }}
                  value={packingNumber}
                />
              </div>
              <div className="mb-3 me-2">
                <Label>Packing Unit</Label>
                <Input
                  id="packingunit"
                  name="packingunit"
                  type="text"
                  onChange={e => {
                    setPackingUnit(e.target.value)
                  }}
                  value={packingUnit}
                />
              </div>
              <div className="mb-3">
                <Label>Packing Style</Label>
                <Input
                  id="packingstyle"
                  name="packingstyle"
                  type="text"
                  onChange={e => {
                    setPackingStyle(e.target.value)
                  }}
                  value={packingStyle}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              className={"btn btn-primary"}
              disabled={!(packingUnit && packingNumber)}
              onClick={async ()=>{
                await props.createPropertyPacking({number: packingNumber, unit: packingUnit, style: packingStyle})
                setAddPackingModal(false)
              }}
            >
              Save
            </button>
          </div>
        </Modal>

        <div className="table-responsive mt-3">
          {
            props.propertiesPacking !== null &&
            props.propertiesPacking.length > 0
              ?
              <ReactTable
                defaultPageSize={5}
                pageSizeOptions={[5, 10]}
                data={props.propertiesPacking}
                columns={columns}
              />
              :
              <NotFound/>
          }
        </div>
      </CardBody>
    </Card>
  )
}


const mapStateToProps = state => {
  const {
    propertiesPacking,
    propertyPackingLoading
  } = state.PropertyPacking
  return {
    propertiesPacking,
    propertyPackingLoading
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      createPropertyPacking,
      updatePropertyPacking,
      deletePropertyPacking,
    }
  )(PackingPropertyTab)
)

PackingPropertyTab.propTypes = {
  createPropertyPacking: PropTypes.func,
  updatePropertyPacking: PropTypes.func,
  deletePropertyPacking: PropTypes.func,
  propertiesPacking: PropTypes.any,
  propertyPackingLoading: PropTypes.bool
}
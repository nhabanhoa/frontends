import React, {lazy, useState} from 'react'
import PropTypes from "prop-types";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"

import NotFound from "_components/common/NotFound";

import {
  createPropertySellBlock,
  deletePropertySellBlock,
  updatePropertySellBlock
} from "_store/properties/sellBlock/actions";

const ReactTable = lazy(() => import('react-table-v6'))

const SellBlockPropertyTab = props => {
  const [addSellBlockModal, setAddSellBlockModal] = useState(false)

  const [sellBlockUnit, setSellBlockUnit] = useState("")
  const [sellBlockNumber, setSellBlockNumber] = useState(0)


  const columns = [
    {
      id: "id",
      Header: "ID",
      accessor: "id",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "number-unit",
      accessor: sellBlock => <div>{`${sellBlock.number} ${sellBlock.unit.toLocaleLowerCase('vi-VN')}`}</div>,
      Header: "SellBlock by Unit",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "action",
      accessor: sellBlock => <button
        className="btn btn-danger"
        onClick={() => {
          props.deletePropertySellBlock(sellBlock.id)
        }}
      >Delete</button>,
      Header: "Action",
      headerClassName: "text-center",
      className: "text-center"
    }
  ]

  return (
    <Card>
      <CardBody className="pt-0">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <CardTitle>SellBlocks</CardTitle>
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target=".modal-center"
            onClick={() => setAddSellBlockModal(true)}
          >
            <i className="bx bx-plus"/>
            Add SellBlocks
          </button>
        </div>

        <Modal
          className="modal-center"
          isOpen={addSellBlockModal}
          toggle={() => setAddSellBlockModal(!addSellBlockModal)}
          centered={true}
        >
          <div className="modal-header">
            <h5 className="modal-title mt-0">Add SellBlock</h5>
            <button
              type="button"
              onClick={() => {
                setAddSellBlockModal(false)
              }}
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <div className="mb-3 me-2">
                <Label>SellBlock Number</Label>
                <Input
                  id="sellblocknumber"
                  name="sellblocknumber"
                  type="number"
                  onChange={e => {
                    setSellBlockNumber(parseInt(e.target.value))
                  }}
                  value={sellBlockNumber}
                />
              </div>
              <div className="mb-3">
                <Label>SellBlock Unit</Label>
                <Input
                  id="sellblockunit"
                  name="sellblockunit"
                  type="text"
                  onChange={e => {
                    setSellBlockUnit(e.target.value)
                  }}
                  value={sellBlockUnit}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              className={"btn btn-primary"}
              disabled={!(sellBlockNumber && sellBlockUnit)}
              onClick={async ()=>{
                await props.createPropertySellBlock({number: sellBlockNumber, unit: sellBlockUnit})
                setAddSellBlockModal(false)
              }}
            >
              Save
            </button>
          </div>
        </Modal>

        <div className="table-responsive mt-3">
          {
            props.propertiesSellBlock !== null &&
            props.propertiesSellBlock.length > 0
              ?
              <ReactTable
                defaultPageSize={5}
                pageSizeOptions={[5, 10]}
                data={props.propertiesSellBlock}
                columns={columns}
              />
              :
              <NotFound/>
          }
        </div>
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => {
  const {
    propertiesSellBlock,
    propertySellBlockLoading
  } = state.PropertySellBlock
  return {
    propertiesSellBlock,
    propertySellBlockLoading
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      createPropertySellBlock,
      updatePropertySellBlock,
      deletePropertySellBlock,
    }
  )(SellBlockPropertyTab)
)

SellBlockPropertyTab.propTypes = {
  createPropertySellBlock: PropTypes.func,
  updatePropertySellBlock: PropTypes.func,
  deletePropertySellBlock: PropTypes.func,
  propertiesSellBlock: PropTypes.any,
  propertySellBlockLoading: PropTypes.bool
}
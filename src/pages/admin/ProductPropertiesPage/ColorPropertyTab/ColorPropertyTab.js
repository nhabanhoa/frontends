import React, {useState, lazy } from 'react'
import PropTypes from 'prop-types'
import {connect} from "react-redux"
import { withRouter } from "react-router-dom"

// import {Card, CardBody, CardTitle, Input, Label, Modal} from "reactstrap"
import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"

import ColorPicker from '_components/common/ColorPicker'
import NotFound from "_components/common/NotFound"

import {
  createPropertyColor,
  updatePropertyColor,
  deletePropertyColor,
  clearColor
} from "_store/properties/color/actions"

// Editable
// import ReactTable from 'react-table-v6'

const ReactTable = lazy(() => import('react-table-v6'))



const ColorPropertyTab = props => {

  const [addColorModal, setAddColorModal] = useState(false)

  const [colorName, setColorName] = useState('BLACK')
  const [colorCode, setColorCode] = useState('#000000')

  const columns = [
    {
      id: "id",
      Header: "ID",
      accessor: "id",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "name",
      accessor: color => <div>{`${color.name.toLocaleUpperCase('vi-VN')}`}</div>,
      Header: "Color Name",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "color",
      accessor: color => <div
        style={{
          backgroundColor: `${color.code}`,
          width: "90%",
          height: "90%"
        }}
      />,
      Header: "Color Code",
      headerClassName: "text-center",
      className: "text-center center-child"
    },
    {
      id: "action",
      accessor: color => <button
        className="btn btn-danger"
        onClick={() => {
          props.deletePropertyColor(color.id)
        }}
      >Delete</button>,
      Header: "Action",
      headerClassName: "text-center",
      className: "text-center"
    }
  ]

  return (
    <Card>
      <CardBody className="pt-0">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <CardTitle>Colors</CardTitle>
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target=".modal-center"
            onClick={() => setAddColorModal(true)}
          >
            <i className="bx bx-plus"/>
            Add Colors
          </button>
        </div>

        <Modal
          className="modal-center"
          isOpen={addColorModal}
          toggle={() => setAddColorModal(!addColorModal)}
          centered={true}
        >
          <div className="modal-header">
            <h5 className="modal-title mt-0">Add Color</h5>
            <button
              type="button"
              onClick={() => {
                setAddColorModal(false)
              }}
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <div className="mb-3 me-2">
                <Label>Color Name</Label>
                <Input
                  id="colorname"
                  name="colorname"
                  type="text"
                  onChange={e => {
                    setColorName(e.target.value.toLocaleUpperCase('vi-VN'))
                  }}
                  value={colorName}
                />
              </div>
              <div className="mb-3">
                <Label>Color Code</Label>
                <ColorPicker
                  color={colorCode}
                  setColorCode={setColorCode}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              className={"btn btn-primary"}
              disabled={!(colorCode && colorName)}
              onClick={async ()=>{
                await props.createPropertyColor({name: colorName, code: colorCode})
                setAddColorModal(false)
              }}
            >
              Save
            </button>
          </div>
        </Modal>

        <div className="table-responsive mt-3">
          {
            props.propertiesColor !== null &&
            props.propertiesColor.length > 0
              ?
                <ReactTable
                  defaultPageSize={5}
                  pageSizeOptions={[5, 10]}
                  data={props.propertiesColor}
                  columns={columns}
                />              
              :
              <NotFound/>
          }
        </div>
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => {
  const {
    propertiesColor,
    propertyColorLoading
  } = state.PropertyColor
  return {
    propertiesColor,
    propertyColorLoading
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      createPropertyColor,
      updatePropertyColor,
      deletePropertyColor,
      clearColor
    }
  )(ColorPropertyTab)
)

ColorPropertyTab.propTypes = {
  createPropertyColor: PropTypes.func,
  updatePropertyColor: PropTypes.func,
  deletePropertyColor: PropTypes.func,
  clearColor: PropTypes.func,
  propertiesColor: PropTypes.any,
  propertyColorLoading: PropTypes.bool
}
import React, {useState, lazy} from 'react'
import PropTypes from "prop-types";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"

import NotFound from "_components/common/NotFound";

import {
  createPropertyType,
  deletePropertyType,
  updatePropertyType
} from "_store/properties/type/actions";

const ReactTable = lazy(() => import('react-table-v6'))
// import ReactTable from 'react-table-v6'

const TypePropertyTab = props => {
  const [addTypeModal, setAddTypeModal] = useState(false)

  const [typeName, setTypeName] = useState("")
  const [typeCode, setTypeCode] = useState("")


  const columns = [
    {
      id: "id",
      Header: "ID",
      accessor: "id",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "code",
      accessor: type => <div>{`${type.code.toLocaleUpperCase('vi-VN')}`}</div>,
      Header: "Type Code",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "name",
      accessor: type => <div>{`${type.name.toLocaleUpperCase('vi-VN')}`}</div>,
      Header: "Type Name",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "action",
      accessor: type => <button
        className="btn btn-danger"
        onClick={() => {
          props.deletePropertyType(type.id)
        }}
      >Delete</button>,
      Header: "Action",
      headerClassName: "text-center",
      className: "text-center"
    }
  ]

  return (
    <Card>
      <CardBody className="pt-0">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <CardTitle>Types</CardTitle>
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target=".modal-center"
            onClick={() => setAddTypeModal(true)}
          >
            <i className="bx bx-plus"/>
            Add Types
          </button>
        </div>

        <Modal
          className="modal-center"
          isOpen={addTypeModal}
          toggle={() => setAddTypeModal(!addTypeModal)}
          centered={true}
        >
          <div className="modal-header">
            <h5 className="modal-title mt-0">Add Type</h5>
            <button
              type="button"
              onClick={() => {
                setAddTypeModal(false)
              }}
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <div className="mb-3 me-2">
                <Label>Type Name</Label>
                <Input
                  id="typename"
                  name="typename"
                  type="text"
                  onChange={e => {
                    setTypeName(e.target.value)
                  }}
                  value={typeName}
                />
              </div>
              <div className="mb-3">
                <Label>Type Code</Label>
                <Input
                  id="typecode"
                  name="typecode"
                  type="text"
                  onChange={e => {
                    setTypeCode(e.target.value)
                  }}
                  value={typeCode}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              className={"btn btn-primary"}
              disabled={!(typeCode && typeName)}
              onClick={async ()=>{
                await props.createPropertyType({name: typeName, code: typeCode})
                setAddTypeModal(false)
              }}
            >
              Save
            </button>
          </div>
        </Modal>

        <div className="table-responsive mt-3">
          {
            props.propertiesType !== null &&
            props.propertiesType.length > 0
              ?
              <ReactTable
                defaultPageSize={5}
                pageSizeOptions={[5, 10]}
                data={props.propertiesType}
                columns={columns}
              />
              :
              <NotFound/>
          }
        </div>
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => {
  const {
    propertiesType,
    propertyTypeLoading
  } = state.PropertyType
  return {
    propertiesType,
    propertyTypeLoading
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      createPropertyType,
      updatePropertyType,
      deletePropertyType,
    }
  )(TypePropertyTab)
)

TypePropertyTab.propTypes = {
  createPropertyType: PropTypes.func,
  updatePropertyType: PropTypes.func,
  deletePropertyType: PropTypes.func,
  propertiesType: PropTypes.any,
  propertyTypeLoading: PropTypes.bool
}
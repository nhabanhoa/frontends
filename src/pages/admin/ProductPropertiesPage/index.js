import React, {useEffect, useState} from "react"
import {withRouter} from "react-router-dom"
import MetaTags from 'react-meta-tags'
import PropTypes from 'prop-types'
import {connect} from "react-redux";
import classnames from "classnames"

import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Col from "_components/react-strap/Col"
import Container from "_components/react-strap/Container"
import Nav from "_components/react-strap/Nav"
import NavItem from "_components/react-strap/NavItem"
import NavLink from "_components/react-strap/NavLink"
import Row from "_components/react-strap/Row"
import TabContent from "_components/react-strap/TabContent"
import TabPane from "_components/react-strap/TabPane"

import ColorPropertyTab from "_pages/admin/ProductPropertiesPage/ColorPropertyTab";
import OriginPropertyType from "_pages/admin/ProductPropertiesPage/OriginPropertyTab";
import PackingPropertyTab from "_pages/admin/ProductPropertiesPage/PackingPropertyTab";
import SellBlockPropertyTab from "_pages/admin/ProductPropertiesPage/SellBlockPropertyTab";
import TypePropertyTab from "_pages/admin/ProductPropertiesPage/TypePropertyTab";

import {getPropertiesColor} from "_store/properties/color/actions";
import {getPropertiesOrigin} from "_store/properties/origin/actions";
import {getPropertiesPacking} from "_store/properties/packing/actions";
import {getPropertiesSellBlock} from "_store/properties/sellBlock/actions";
import {getPropertiesType} from "_store/properties/type/actions";
import {isEmptyArray} from "_utils/helpers";

const ProductPropertiesPage = props => {

  const {
    propertiesColor,
    propertiesOrigin,
    propertiesPacking,
    propertiesSellBlock,
    propertiesType,
    getPropertiesColor,
    getPropertiesOrigin,
    getPropertiesPacking,
    getPropertiesSellBlock,
    getPropertiesType
  } = props;

  const [activeTab, setActiveTab] = useState("1")

  useEffect(() => {
    if (isEmptyArray(propertiesColor)) getPropertiesColor()
    if (isEmptyArray(propertiesOrigin)) getPropertiesOrigin()
    if (isEmptyArray(propertiesPacking)) getPropertiesPacking()
    if (isEmptyArray(propertiesSellBlock)) getPropertiesSellBlock()
    if (isEmptyArray(propertiesType)) getPropertiesType()
  }, [])

  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>Product Properties Page</title>
        </MetaTags>
        <Container fluid={true}>
          <Row>
            <Col xs="12" md="12" lg="12">
              <Card>
                <CardBody>
                  <CardTitle className="h4 mb-3">Product Properties</CardTitle>
                  <Row>
                    <Col md="2">
                      <Nav className="flex-column vertical-icon">
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              'mb-2': true,
                              active: activeTab === "1"
                            })}
                            onClick={() => {
                              setActiveTab("1")
                            }}
                          ><i className="bx bx-palette"/> Color
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              'mb-2': true,
                              active: activeTab === "2"
                            })}
                            onClick={() => {
                              setActiveTab("2")
                            }}
                          ><i className="bx bx-globe"/> Origin
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              'mb-2': true,
                              active: activeTab === "3",
                            })}
                            onClick={() => {
                              setActiveTab("3")
                            }}
                          ><i className="bx bx-package"/> Packing
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              'mb-2': true,
                              active: activeTab === "4"
                            })}
                            onClick={() => {
                              setActiveTab("4")
                            }}
                          ><i className="bx bx-cart"/> Sell Block
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ cursor: "pointer" }}
                            className={classnames({
                              active: activeTab === "5"
                            })}
                            onClick={() => {
                              setActiveTab("5")
                            }}
                          ><i className="bx bx-food-menu"/> Type
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </Col>
                    <Col md="10">
                      <TabContent activeTab={activeTab} className="text-muted mt-4 mt-md-0">
                        <TabPane tabId="1">
                          <ColorPropertyTab/>
                        </TabPane>
                        <TabPane tabId="2">
                          <OriginPropertyType/>
                        </TabPane>
                        <TabPane tabId="3">
                          <PackingPropertyTab/>
                        </TabPane>

                        <TabPane tabId="4">
                          <SellBlockPropertyTab/>
                        </TabPane>
                        <TabPane tabId="5">
                          <TypePropertyTab/>
                        </TabPane>
                      </TabContent>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>

    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const {propertiesColor} = state.PropertyColor
  const {propertiesOrigin} = state.PropertyOrigin
  const {propertiesPacking} = state.PropertyPacking
  const {propertiesSellBlock} = state.PropertySellBlock
  const {propertiesType} = state.PropertyType
  return {
    propertiesColor,
    propertiesOrigin,
    propertiesPacking,
    propertiesSellBlock,
    propertiesType
  }
}

// export default ProductPropertiesPage

export default withRouter(
  connect(mapStateToProps, {
    getPropertiesColor,
    getPropertiesOrigin,
    getPropertiesPacking,
    getPropertiesSellBlock,
    getPropertiesType,
  })(ProductPropertiesPage)
)

ProductPropertiesPage.propTypes = {
  getPropertiesColor: PropTypes.func,
  getPropertiesOrigin: PropTypes.func,
  getPropertiesPacking: PropTypes.func,
  getPropertiesSellBlock: PropTypes.func,
  getPropertiesType: PropTypes.func,
  propertiesColor: PropTypes.array,
  propertiesOrigin: PropTypes.array,
  propertiesPacking: PropTypes.array,
  propertiesSellBlock: PropTypes.array,
  propertiesType: PropTypes.array
}


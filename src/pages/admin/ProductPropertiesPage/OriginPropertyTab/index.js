import React from "react";
import loadable from "_utils/loadable"

export default loadable(() => import("./OriginPropertyTab"), {
    fallback: <div>...</div>
})
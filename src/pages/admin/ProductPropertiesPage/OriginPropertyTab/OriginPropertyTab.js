import React, {lazy, useState} from 'react'
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"

import NotFound from "_components/common/NotFound";

import {
  createPropertyOrigin,
  deletePropertyOrigin,
  updatePropertyOrigin
} from "_store/properties/origin/actions";

const ReactTable = lazy(() => import('react-table-v6'))

const OriginPropertyTab = props => {
  const [addOriginModal, setAddOriginModal] = useState(false)

  const [originName, setOriginName] = useState("")
  const [originCode, setOriginCode] = useState("")


  const columns = [
    {
      id: "id",
      Header: "ID",
      accessor: "id",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "code",
      accessor: origin => <div>{`${origin.code.toLocaleUpperCase('vi-VN')}`}</div>,
      Header: "Origin Code",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "name",
      accessor: origin => <div>{`${origin.name.toLocaleUpperCase('vi-VN')}`}</div>,
      Header: "Origin Name",
      headerClassName: "text-center",
      className: "text-center cell-padding-top"
    },
    {
      id: "action",
      accessor: origin => <button
        className="btn btn-danger"
        onClick={() => {
          props.deletePropertyOrigin(origin.id)
        }}
      >Delete</button>,
      Header: "Action",
      headerClassName: "text-center",
      className: "text-center"
    }
  ]

  return (
    <Card>
      <CardBody className="pt-0">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <CardTitle>Origins</CardTitle>
          <button
            className="btn btn-primary"
            data-toggle="modal"
            data-target=".modal-center"
            onClick={() => setAddOriginModal(true)}
          >
            <i className="bx bx-plus"/>
            Add Origins
          </button>
        </div>

        <Modal
          className="modal-center"
          isOpen={addOriginModal}
          toggle={() => setAddOriginModal(!addOriginModal)}
          centered={true}
        >
          <div className="modal-header">
            <h5 className="modal-title mt-0">Add Origin</h5>
            <button
              type="button"
              onClick={() => {
                setAddOriginModal(false)
              }}
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <div className="mb-3 me-2">
                <Label>Origin Name</Label>
                <Input
                  id="originname"
                  name="originname"
                  type="text"
                  onChange={e => {
                    setOriginName(e.target.value)
                  }}
                  value={originName}
                />
              </div>
              <div className="mb-3">
                <Label>Origin Code</Label>
                <Input
                  id="origincode"
                  name="origincode"
                  type="text"
                  onChange={e => {
                    setOriginCode(e.target.value)
                  }}
                  value={originCode}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              className={"btn btn-primary"}
              disabled={!(originCode && originName)}
              onClick={async ()=>{
                await props.createPropertyOrigin({name: originName, code: originCode})
                setAddOriginModal(false)
              }}
            >
              Save
            </button>
          </div>
        </Modal>

        <div className="table-responsive mt-3">
          {
            props.propertiesOrigin !== null &&
            props.propertiesOrigin.length > 0
              ?
              <ReactTable
                defaultPageSize={5}
                pageSizeOptions={[5, 10]}
                data={props.propertiesOrigin}
                columns={columns}
              />
              :
              <NotFound/>
          }
        </div>
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => {
  const {
    propertiesOrigin,
    propertyOriginLoading
  } = state.PropertyOrigin
  return {
    propertiesOrigin,
    propertyOriginLoading
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    {
      createPropertyOrigin,
      updatePropertyOrigin,
      deletePropertyOrigin,
    }
  )(OriginPropertyTab)
)

OriginPropertyTab.propTypes = {
  createPropertyOrigin: PropTypes.func,
  updatePropertyOrigin: PropTypes.func,
  deletePropertyOrigin: PropTypes.func,
  propertiesOrigin: PropTypes.any,
  propertyOriginLoading: PropTypes.bool
}
import React, {useEffect, useState} from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import MetaTags from 'react-meta-tags';
import PropTypes from 'prop-types'
import {error, warn, info, success} from '_utils/custom-toastify'
import { withTranslation } from "react-i18next"
import classnames from "classnames"

import Button from "_components/react-strap/Button"
import Card from "_components/react-strap/Card"
import CardBody from "_components/react-strap/CardBody"
import CardTitle from "_components/react-strap/CardTitle"
import Col from "_components/react-strap/Col"
import Container from "_components/react-strap/Container"
import Form from "_components/react-strap/Form"
import Input from "_components/react-strap/Input"
import Label from "_components/react-strap/Label"
import Modal from "_components/react-strap/Modal"
import Row from "_components/react-strap/Row"
import Select from "_components/common/Select"

import {
  createProduct, 
  getPropertiesOrigin, 
  getPropertiesPacking, 
  getPropertiesSellBlock, 
  getPropertiesType,
} from "_store/actions"

import {
  uploadImageS3,
  getPropertiesColor,
} from "_store/actions"

import {uploadFilesForProductS3} from "_utils/storage/s3";
import {isEmptyArray} from "_utils/helpers";
import loadable from "_utils/loadable";

const CurrencyFormat = loadable(() => import("react-currency-format"), {
  fallback: <div>...</div>
})

const ImageSelectorModalBody = loadable(() => import('./ImageSelectorModalBody'), {
  fallback: <div>...</div>
}) 


const AddProductPage = props => {

  const {
    propertiesColor,
    propertiesOrigin,
    propertiesPacking,
    propertiesSellBlock,
    propertiesType,
    getPropertiesColor,
    getPropertiesOrigin,
    getPropertiesPacking,
    getPropertiesSellBlock,
    getPropertiesType
  } = props;

  const [isImageModelOpen, setIsImageModelOpen] = useState(false)

  const [type, setType] = useState(null)
  const [colors, setColors] = useState([])
  const [origin, setOrigin] = useState(null)
  const [packing, setPacking] = useState(null)
  const [sellBlock, setSellBlock] = useState(null)
  const [productName, setProductName] = useState("")
  const [sizePrices, setSizePrices] = useState([{size: null, price: 0}])
  const [retailPrice, setRetailPrice] = useState(null)
  const [selectedFiles, setSelectedFiles] = useState([])
  const [productDisplayName, setProductDisplayName] = useState("")

  const sizeOptions = [
    { value: "30cm", label: "30cm" },
    { value: "40cm", label: "40cm" },
    { value: "50cm", label: "50cm" },
    { value: "60cm", label: "60cm" },
    { value: "70cm", label: "70cm" },
    { value: "80cm", label: "80cm" },
    { value: "90cm", label: "90cm" },
    { value: "100cm", label: "100cm" },
    { value: "120cm", label: "120cm" },
    { value: "150cm", label: "150cm" },
    { value: "200cm", label: "200cm" },
    { value: "250cm", label: "250cm" },
    { value: "300cm", label: "300cm" }
  ]

  useEffect(() => {
    if (isEmptyArray(propertiesColor)) getPropertiesColor()
    if (isEmptyArray(propertiesOrigin)) getPropertiesOrigin()
    if (isEmptyArray(propertiesPacking)) getPropertiesPacking()
    if (isEmptyArray(propertiesSellBlock)) getPropertiesSellBlock()
    if (isEmptyArray(propertiesType)) getPropertiesType()
  }, [])

  const handleSubmit = async (e) =>{
    e.preventDefault()

    const response = await uploadFilesForProductS3(selectedFiles)
    console.log('image response', response)

    // props.uploadImageS3(selectedFiles)

    // props.createProduct({
    //   name: productName,
    //   displayName: productDisplayName,
    //   retailPrice: retailPrice,
    //   packingId: packing,
    //   sellBlockId: sellBlock,
    //   colorIds: colors,
    //   typeId: type,
    //   originId: origin,
    //   sizesPrices: sizesPrices
    // })

    // console.log({
    //   name: productName,
    //   images: [response],
    //   displayName: productDisplayName,
    //   retailPrice: retailPrice,
    //   packingId: packing,
    //   sellBlockId: sellBlock,
    //   colorIds: colors,
    //   typeId: type,
    //   originId: origin,
    //   sizePrices: sizePrices
    // })

    props.createProduct({
      name: productName,
      imageNames: [response],
      displayName: productDisplayName,
      retailPrice: retailPrice,
      packingId: packing,
      sellBlockId: sellBlock,
      colorIds: colors,
      typeId: type,
      originId: origin,
      sizePrices: sizePrices
    })
  }

  const handleAcceptedFiles = (files) => {
    if (files.length > 1) {
      error("Do not accept multiple file!!!")
      return
    }
    if (files[0].type.match(/.(gif|jpe?g|tiff?|png|webp|bmp)$/i) === null) {
      error("File is not image!!!")
      return
    }
    files.map(file =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size)
      })
    )

    console.log(files)
    setSelectedFiles(files)
  }

  function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return "0 Bytes"
    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]

    const i = Math.floor(Math.log(bytes) / Math.log(k))
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i]
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>{props.t("product.add")}</title>
        </MetaTags>
        <Container fluid>
          <Row>
            <Col xs="12" md="12" lg="4">
              <Card>
                <CardBody>
                  <CardTitle className="mb-3">{props.t("product.image")}</CardTitle>
                  <Form>
                    <div  className="custom-dropzone" onClick={() => setIsImageModelOpen(true)}>
                      <i className="display-1 bx bx-image"/>
                    </div>
                    <Modal
                      size="xl"
                      isOpen={isImageModelOpen}
                      toggle={() => {
                        setIsImageModelOpen(false)
                      }}
                    >
                      <div className="modal-header">
                        <h5
                          className="modal-title mt-0"
                          id="myExtraLargeModalLabel"
                        >
                          Image Editor
                        </h5>
                        <button
                          onClick={() => {
                            setIsImageModelOpen(false)
                          }}
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <ImageSelectorModalBody/>
                    </Modal>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" md="12" lg="8">
              <Card>
                <CardBody>
                  <CardTitle>Basic Information</CardTitle>
                  <Form onSubmit={handleSubmit}>
                    <Row>
                      <Col sm="6">
                        <div className="mb-3">
                          <Label htmlFor="productname">Product Name</Label>
                          <Input
                            id="productname"
                            name="productname"
                            type="text"
                            className="form-control"
                            onChange={e => { if(e != null && e.target != null) setProductName(e.target.value)
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          <Label className="control-label">Colors</Label>
                          <Select
                            isClearable={true}
                            isSearchable={true}
                            classNamePrefix="select2-selection"
                            placeholder="Choose..."
                            title="Colors"
                            options={props.propertiesColor.map(item => ({value: item.id, label: item.name}))}
                            isMulti
                            onChange={colors => { if (colors != null) setColors(colors.map(item => item.value))
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          <Label className="control-label">Origin</Label>
                          <Select
                            isClearable={true}
                            isSearchable={true}
                            classNamePrefix="select2-selection"
                            placeholder="Choose..."
                            title="Origin"
                            options={props.propertiesOrigin.map(item => ({value: item.id, label: item.name}))}
                            onChange={origin => { if (origin != null) setOrigin(origin.value)
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          <Label className="control-label">Sell Block</Label>
                          <Select
                            isClearable={true}
                            isSearchable={true}
                            classNamePrefix="select2-selection"
                            placeholder="Choose..."
                            title="SellBlock"
                            options={props.propertiesSellBlock.map(item => ({value: item.id, label: `${item.number} ${item.unit}`}))}
                            onChange={sellBlock => { if (sellBlock != null) setSellBlock(sellBlock.value)
                            }}
                          />
                        </div>
                      </Col>

                      <Col sm="6">
                        <div className="mb-3">
                          <Label htmlFor="productdisplayname">Product Display Name</Label>
                          <Input
                            id="productdisplayname"
                            name="productdisplayname"
                            type="text"
                            className="form-control"
                            onChange={e => {if(e != null && e.target != null) setProductDisplayName(e.target.value)}}
                          />
                        </div>
                        <div className="mb-3">
                          <Label className="control-label">Type</Label>
                          <Select
                            isClearable={true}
                            isSearchable={true}
                            classNamePrefix="select2-selection"
                            placeholder="Choose..."
                            title="Type"
                            options={props.propertiesType.map(item => ({value: item.id, label: item.name}))}
                            onChange={type => { if (type != null) setType(type.value)
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          <Label className="control-label">Packing</Label>
                          <Select
                            isClearable={true}
                            isSearchable={true}
                            classNamePrefix="select2-selection"
                            placeholder="Choose..."
                            title="Packing"
                            options={props.propertiesPacking.map(item => ({value: item.id, label: `${item.number} ${item.unit}/${item.style}`}))}
                            onChange={packing => {
                              if (packing != null) setPacking(packing.value)
                            }}
                          />
                        </div>
                        <div className="mb-3">
                          <Label htmlFor="retailprice">Retail Price</Label>
                          <CurrencyFormat
                            thousandSeparator={'.'}
                            decimalSeparator={','}
                            suffix={' vnđ'}
                            customInput={Input}
                            step={1000}
                            min={0}
                            className="form-control"
                            id="retailprice"
                            name="retailprice"
                            // type="number"
                            onValueChange={objVal => {
                              if (objVal != null) setRetailPrice(objVal.value)
                            }}
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <Label className="control-label">Sizes - Prices</Label>
                          <Row key='size-prices-label'>
                            <Col sm='5'><Label className="control-label mb-2">Sizes</Label></Col>
                            <Col sm='5'><Label className="control-label mb-2">Prices</Label></Col>
                            <Col sm="2"></Col>
                          </Row>
                        {
                          sizePrices ?
                          sizePrices.map((x, i) => 
                            // console.log('size-prices ', i, x)
                            <Row key={`size-prices-${i}`}>                              
                              <Col sm="5">
                                <div className="mb-3" key={`size-${i}`}>
                                  <Select
                                    key={`size-${i}`}
                                    isClearable={true}
                                    isSearchable={true}
                                    classNamePrefix="select2-selection"
                                    placeholder="Choose..."
                                    options={sizeOptions}
                                    // value={sizePrices[i].size}
                                    onChange={e => {
                                      let temp = [...sizePrices]
                                      temp[i].size = e.value
                                      setSizePrices(temp)
                                    }}
                                  />
                                </div>
                              </Col>
                              <Col sm="5">                                
                                <CurrencyFormat
                                  key={`price-${i}`}
                                  thousandSeparator={'.'}
                                  decimalSeparator={','}
                                  suffix={' vnđ'}
                                  customInput={Input}
                                  className="form-control mb-3"
                                  name={`price${i}`}
                                  // value={sizePrices[i].price}
                                  onValueChange={objVal => {
                                    if (objVal != null) {
                                      let temp = [...sizePrices]
                                      temp[i].price = objVal.value
                                      setSizePrices(temp)
                                    }
                                    
                                  }}
                                />
                              </Col>
                              <Col sm="2" className='size-price-delete-btn'>
                                <Button
                                  color="danger"
                                  // className={classnames('btn btn-danger', {'mt-4': i === 0})}
                                  className='btn btn-danger'
                                  onClick={() => {
                                    let temp = [...sizePrices]
                                    // temp.splice(i, 1)
                                    console.log('temp', temp)
                                    console.log('i', i)
                                    temp.splice(i, 1)
                                    console.log('temp-1', temp)
                                    setSizePrices(temp)
                                  }}
                                >
                                  <i className="bx bx-x label-icon"></i>
                                </Button>
                              </Col>
                            </Row>
                          ) :
                          null
                        }
                        <Row>
                          <button
                            type="button"
                            className="btn btn-primary btn-label"
                            onClick={() => {
                              setSizePrices([...sizePrices, {size: null, price: 0}])
                            }}
                          >
                            <i className="bx bx-plus label-icon"></i>Add Size-Price
                          </button>
                        </Row>
                      </Col>
                      <Col sm="6">
                        <div className="d-flex flex-wrap gap-2 custom-button-group">
                          <button
                            type="submit"
                            className="btn btn-success btn-label"
                            disabled={props.productLoading}
                          >
                            <i className="bx bx-plus label-icon"></i>Add New
                          </button>
                          <button
                            type="button"
                            className="btn btn-danger btn-label"
                            disabled={props.productLoading}
                          >
                            <i className="bx bx-x label-icon"></i>Cancel
                          </button>
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const { productLoading } = state.Product
  const { propertiesColor } = state.PropertyColor
  const { propertiesOrigin } = state.PropertyOrigin
  const { propertiesPacking } = state.PropertyPacking
  const { propertiesSellBlock } = state.PropertySellBlock
  const { propertiesType } = state.PropertyType
  const { s3Error, s3Loading } = state.S3
  return {
    productLoading,
    s3Error,
    s3Loading,
    propertiesColor,
    propertiesOrigin,
    propertiesPacking,
    propertiesSellBlock,
    propertiesType
  }
}

export default withRouter(
  connect(mapStateToProps, {
    createProduct,
    uploadImageS3,
    getPropertiesColor,
    getPropertiesOrigin,
    getPropertiesPacking,
    getPropertiesSellBlock,
    getPropertiesType,
  })(withTranslation()(AddProductPage))
)

AddProductPage.propTypes = {
  s3Error: PropTypes.any,
  history: PropTypes.object,
  uploadImageS3: PropTypes.func,
  createProduct: PropTypes.func,
  getPropertiesColor: PropTypes.func,
  getPropertiesOrigin: PropTypes.func,
  getPropertiesPacking: PropTypes.func,
  getPropertiesSellBlock: PropTypes.func,
  getPropertiesType: PropTypes.func,
  propertiesColor: PropTypes.array,
  propertiesOrigin: PropTypes.array,
  propertiesPacking: PropTypes.array,
  propertiesSellBlock: PropTypes.array,
  propertiesType: PropTypes.array,
  productLoading: PropTypes.bool
}
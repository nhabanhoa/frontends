import React, {useState} from "react";
import classnames from "classnames"

import Col from "_components/react-strap/Col"
import Nav from "_components/react-strap/Nav"
import NavItem from "_components/react-strap/NavItem"
import NavLink from "_components/react-strap/NavLink"
import Row from "_components/react-strap/Row"
import TabContent from "_components/react-strap/TabContent"
import TabPane from "_components/react-strap/TabPane"
import loadable from "_utils/loadable";

const UploadImageTab = loadable(() => import("./UploadImageTab"), {fallback: <div>...</div>});
const SelectImageTab = loadable(() => import("./SelectImageTab"), {fallback: <div>...</div>});

const ImageSelectorModalBody = () => {

	const [customActiveTab, setCustomActiveTab] = useState('1')

  return (
    <div className="modal-body custom-modal-body">
      <Row>
        <Col lg={12}>
          <Nav tabs className="nav-tabs-custom nav-justified">
            <NavItem>
              <NavLink
                style={{ cursor: "pointer" }}
                className={classnames({
                  active: customActiveTab === "1",
                })}
                onClick={() => {
                  setCustomActiveTab("1");
                }}
              >
                <span className="d-block d-sm-none">
                  <i className="bx bx-home-circle"></i>
                </span>
                <span className="d-none d-sm-block">Choose Image</span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                style={{ cursor: "pointer" }}
                className={classnames({
                  active: customActiveTab === "2",
                })}
                onClick={() => {
                  setCustomActiveTab("2");
                }}
              >
                <span className="d-block d-sm-none">
                  <i className="bx bx-user"></i>
                </span>
                <span className="d-none d-sm-block">Upload Image</span>
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={customActiveTab} className="p-3 text-muted">
            <TabPane tabId="1">
              <UploadImageTab/>
            </TabPane>
            <TabPane tabId="2">
              <SelectImageTab/>
            </TabPane>
          </TabContent>
        </Col>
      </Row>
    </div>
  );
};

export default ImageSelectorModalBody;

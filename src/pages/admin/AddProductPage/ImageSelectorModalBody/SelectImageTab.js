import React from "react";
import { connect } from "react-redux"
import { withTranslation } from "react-i18next"

import Col from "_components/react-strap/Col"
import Row from "_components/react-strap/Row"

const SelectImageTab = () => {
  return (
    <Row>
      <Col sm="12">
        Raw denim you probably haven't heard of them jean shorts Austin.
        Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache
        cliche tempor, williamsburg carles vegan helvetica. Reprehenderit
        butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui
        irure terry richardson ex squid. Aliquip placeat salvia cillum iphone.
        Seitan aliquip quis cardigan american apparel, butcher voluptate nisi
        qui.
      </Col>
    </Row>
  );
};

const mapStateToProps = state => {
  // const { productImage } = state.Media
	// return {
	// 	productImage
	// }
}

export default connect(mapStateToProps, {
})(withTranslation()(SelectImageTab));

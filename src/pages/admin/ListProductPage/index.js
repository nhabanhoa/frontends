import React, { useEffect, useState } from "react"
import MetaTags from 'react-meta-tags';
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

import { Button, Card, CardBody, Col, Container, Row } from "reactstrap"
import ProductCard  from "_components/common/ProductCard"

import InfiniteScroll from 'react-infinite-scroll-component'
import {withTranslation} from "react-i18next";

//Import Breadcrumb
import Breadcrumbs from "_components/common/Breadcrumb"
import { getProducts } from "_store/actions"
// import ListProductPageColumns from "./EcommerceOrderColumns"
// import ListProductPageModal from "./ListProductPageModal"

const ListProductPage = props => {

  useEffect(() => {
    props.getProducts({
      page: 1,
      size: 10,
    })
  }, [])
  
  return (
    <React.Fragment>
      <div className="page-content">
        <MetaTags>
          <title>List Product</title>
        </MetaTags>
        <Container fluid>
          <Row>
            {
              props.products != null && props.products.content != null ?
              props.products.content.map((prod, idx) => <Col xl="4" sm="6" key={`product_${idx}`}><ProductCard product={prod}/></Col>) :
              null
            }
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const {products, productLoading} = state.Product
  return {
    products,
    productLoading
  }
}

export default withRouter(
  connect(mapStateToProps, {
    getProducts
  }
)(withTranslation()(ListProductPage)))

ListProductPage.propTypes = {
  getProducts: PropTypes.func,
  products: PropTypes.object,
  productLoading: PropTypes.bool
}
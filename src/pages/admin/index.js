//Route
import AdminRoute from '_router/AdminRoute'

//Layout
import AdminLayout from '_layout/admin'

//Pages
import DashboardPage from '_pages/admin/Dashboard'
import ListProductPage from "_pages/admin/ListProductPage"
import AddProductPage from '_pages/admin/AddProductPage'
import ProductPropertiesPage from '_pages/admin/ProductPropertiesPage'
import ListImagePage from '_pages/admin/ListImagePage'

const routeConfig = {
  layout: AdminLayout,
  route: AdminRoute
}

console.log("DashboardPage",DashboardPage)
console.log("ListProductPage",ListProductPage)
console.log("AddProductPage",AddProductPage)
console.log("ProductPropertiesPage",ProductPropertiesPage)
console.log("ListImagePage",ListImagePage)

const AdminRoutes = [
  {
    ...routeConfig,
    title: "Dashboard",
    component: DashboardPage,
    path: '/admin/dashboard',
  },
  {
    ...routeConfig,
    title: "",
    component: ListProductPage,
    path: '/admin/list-product',
  },
  {
    ...routeConfig,
    title: "",
    component: AddProductPage,
    path: "/admin/create-product"
  },
  {
    ...routeConfig,
    title: "",
    component: ProductPropertiesPage,
    path: "/admin/product-properties"
  },
  {
    ...routeConfig,
    title: "",
    component: ListImagePage,
    path: "/admin/list-images"
  }
]

export default AdminRoutes
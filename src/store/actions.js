export * from "./layout/actions"

// Authentication module
export * from "./auth/register/actions"
export * from "./auth/login/actions"
export * from "./auth/forgetpwd/actions"
export * from "./profile/actions"
export * from "./product/actions"
export * from "./s3/actions"
// export * from "./properties/actions"
export * from "./properties/color/actions"
export * from "./properties/origin/actions"
export * from "./properties/packing/actions"
export * from "./properties/sellBlock/actions"
export * from "./properties/type/actions"
// //Ecommerce
// export * from "./e-commerce/actions"
//
// //Calendar
// export * from "./calendar/actions"
//
// //chat
// export * from "./chat/actions"
//
// //crypto
// export * from "./crypto/actions"
//
// //invoices
// export * from "./invoices/actions"
//
// // projects
// export * from "./projects/actions"
//
// // tasks
// export * from "./tasks/actions"
//
// // contacts
// export * from "./contacts/actions"

import { combineReducers } from "redux"

// Front
import Layout from "./layout/reducer"

// Authentication
import Login from "./auth/login/reducer"
import Account from "./auth/register/reducer"
import ForgetPassword from "./auth/forgetpwd/reducer"
import Profile from "./profile/reducer"

//Product
import Product from "_store/product/reducer";

//S3
import S3 from "_store/s3/reducer"

//Product properties
// import Property from "_store/properties/reducer"

import PropertyColor from "_store/properties/color/reducer"
import PropertyOrigin from "_store/properties/origin/reducer"
import PropertyPacking from "_store/properties/packing/reducer"
import PropertySellBlock from "_store/properties/sellBlock/reducer"
import PropertyType from "_store/properties/type/reducer"
// //E-commerce
// import ecommerce from "./e-commerce/reducer"
//
// //Calendar
// import calendar from "./calendar/reducer"
//
// //chat
// import chat from "./chat/reducer"
//
// //crypto
// import crypto from "./crypto/reducer"
//
// //invoices
// import invoices from "./invoices/reducer"
//
// //projects
// import projects from "./projects/reducer"
//
// //tasks
// import tasks from "./tasks/reducer"
//
// //contacts
// import contacts from "./contacts/reducer"

const rootReducer = combineReducers({
  // public
  Layout,
  Login,
  Account,
  ForgetPassword,
  Profile,
  Product,
  S3,
  PropertyColor,
  PropertyOrigin,
  PropertyPacking,
  PropertySellBlock,
  PropertyType
  // Property
  // ecommerce,
  // calendar,
  // chat,
  // crypto,
  // invoices,
  // projects,
  // tasks,
  // contacts,
})

export default rootReducer

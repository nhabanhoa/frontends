import {
  GET_COLORS_FAIL,
  GET_COLORS_SUCCESS,
  GET_COLOR_DETAIL_SUCCESS,
  GET_COLOR_DETAIL_FAIL,
} from "./actionTypes"

const INIT_STATE = {
  colors: [],
  color: {},
}

const Product = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_COLORS_SUCCESS:
      return {
        ...state,
        products: action.payload,
      }

    case GET_COLORS_FAIL:
      return {
        ...state,
        error: action.payload,
      }

    case GET_COLOR_DETAIL_SUCCESS:
      return {
        ...state,
        product: action.payload,
      }

    case GET_COLOR_DETAIL_FAIL:
      return {
        ...state,
        error: action.payload,
      }

    default:
      return state
  }
}

export default Product

import {
  GET_COLORS,
  GET_COLORS_FAIL,
  GET_COLORS_SUCCESS,
  GET_COLOR_DETAIL,
  GET_COLOR_DETAIL_FAIL,
  GET_COLOR_DETAIL_SUCCESS,
} from "./actionTypes"

export const getColors = () => ({
  type: GET_COLORS,
})

export const getColorsSuccess = colors => ({
  type: GET_COLORS_SUCCESS,
  payload: colors,
})

export const getColorsFail = error => ({
  type: GET_COLORS_FAIL,
  payload: error,
})

export const getColorDetail = colorId => ({
  type: GET_COLOR_DETAIL,
  colorId,
})

export const getColorDetailSuccess = colors => ({
  type: GET_COLOR_DETAIL_SUCCESS,
  payload: colors,
})

export const getColorDetailFail = error => ({
  type: GET_COLOR_DETAIL_FAIL,
  payload: error,
})

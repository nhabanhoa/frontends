import { call, put, takeEvery } from "redux-saga/effects"

// Ecommerce Redux States
import {
  GET_COLOR_DETAIL,
  GET_COLORS,
} from "./actionTypes"

import {
  getColorDetailFail,
  getColorDetailSuccess,
  getColorsFail,
  getColorsSuccess,
} from "./actions"

//Helpers
import {
  getColors,
  getColorDetail
} from "_api/product"

function* fetchColors() {
  try {
    const response = yield call(getColors)
    if (response.status === 200){
      yield put(getColorsSuccess(response.data))
    } else {
      throw response.message
    }
  } catch (error) {
    yield put(getColorsFail(error))
  }
}

function* fetchColorDetail(productId) {
  try {
    const response = yield call(getColorDetail, productId)
    if (response.status === 200){
      yield put(getColorDetailSuccess(response.data))
    } else {
      throw response.message
    }
  } catch (error) {
    yield put(getColorDetailFail(error))
  }
}

function* ColorSaga() {
  yield takeEvery(GET_COLORS, fetchColors)
  yield takeEvery(GET_COLOR_DETAIL, fetchColorDetail)
}

export default ColorSaga

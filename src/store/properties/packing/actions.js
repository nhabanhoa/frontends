import {
  CREATE_PROPERTY_PACKING,
  CREATE_PROPERTY_PACKING_FAIL,
  CREATE_PROPERTY_PACKING_SUCCESS,
  UPDATE_PROPERTY_PACKING,
  UPDATE_PROPERTY_PACKING_FAIL,
  UPDATE_PROPERTY_PACKING_SUCCESS,
  GET_PROPERTIES_PACKING,
  GET_PROPERTIES_PACKING_FAIL,
  GET_PROPERTIES_PACKING_SUCCESS,
  DELETE_PROPERTY_PACKING,
  DELETE_PROPERTY_PACKING_FAIL,
  DELETE_PROPERTY_PACKING_SUCCESS
} from "./actionTypes"

export const createPropertyPacking = (propertyInfo) => ({
  type: CREATE_PROPERTY_PACKING,
  payload: {propertyInfo}
})

export const createPropertyPackingFail = error => ({
  type: CREATE_PROPERTY_PACKING_FAIL,
  payload: error
})

export const createPropertyPackingSuccess = property => ({
  type: CREATE_PROPERTY_PACKING_SUCCESS,
  payload: property
})

export const updatePropertyPacking = property => ({
  type: UPDATE_PROPERTY_PACKING,
  payload: {property}
})

export const updatePropertyPackingFail = error => ({
  type: UPDATE_PROPERTY_PACKING_FAIL,
  payload: error
})

export const updatePropertyPackingSuccess = property => ({
  type: UPDATE_PROPERTY_PACKING_SUCCESS,
  payload: property
})

export const getPropertiesPacking = () => ({
  type: GET_PROPERTIES_PACKING,
})

export const getPropertiesPackingFail = error => ({
  type: GET_PROPERTIES_PACKING_FAIL,
  payload: error
})

export const getPropertiesPackingSuccess = properties => ({
  type: GET_PROPERTIES_PACKING_SUCCESS,
  payload: properties
})

export const deletePropertyPacking = (id) => ({
  type: DELETE_PROPERTY_PACKING,
  payload: {id}
})

export const deletePropertyPackingFail = error => ({
  type: DELETE_PROPERTY_PACKING_FAIL,
  payload: error
})

export const deletePropertyPackingSuccess = info => ({
  type: DELETE_PROPERTY_PACKING_SUCCESS,
  payload: info
})
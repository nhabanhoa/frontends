import {
  GET_PROPERTIES_PACKING,
  GET_PROPERTIES_PACKING_FAIL,
  GET_PROPERTIES_PACKING_SUCCESS,
  CREATE_PROPERTY_PACKING,
  CREATE_PROPERTY_PACKING_FAIL,
  CREATE_PROPERTY_PACKING_SUCCESS,
  UPDATE_PROPERTY_PACKING,
  UPDATE_PROPERTY_PACKING_FAIL,
  UPDATE_PROPERTY_PACKING_SUCCESS,
  DELETE_PROPERTY_PACKING,
  DELETE_PROPERTY_PACKING_FAIL,
  DELETE_PROPERTY_PACKING_SUCCESS
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const initialState = {
  propertiesPacking: [],
  propertyPackingLoading: false
}

const propertyPacking = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES_PACKING:
      return {
        ...state,
        propertyPackingLoading: true
      }
    case GET_PROPERTIES_PACKING_FAIL:{
      error(action.payload)
      return {
        ...state,
        propertyPackingLoading: false,
        propertiesPacking: []
      }
    }
    case GET_PROPERTIES_PACKING_SUCCESS:{
      success("Packings loaded!!!")
      return {
        ...state,
        propertyPackingLoading: false,
        propertiesPacking: action.payload
      }
    }
    case CREATE_PROPERTY_PACKING:
      return {
        ...state,
        propertyPackingLoading: true
      }
    case CREATE_PROPERTY_PACKING_FAIL:{
      error("Cannot created origin! " + action.payload)
      return {
        ...state,
        propertyPackingLoading: false
      }
    }
    case CREATE_PROPERTY_PACKING_SUCCESS:{
      success("Packing created.")
      return {
        ...state,
        propertiesPacking: state.propertiesPacking.concat(action.payload),
        propertyPackingLoading: false
      }
    }
    case UPDATE_PROPERTY_PACKING:
      return {
        ...state,
        propertyPackingLoading: true
      }
    case UPDATE_PROPERTY_PACKING_FAIL:
      return {
        ...state,
        propertyPackingLoading: false
      }
    case UPDATE_PROPERTY_PACKING_SUCCESS:
      return {
        ...state,
        propertyPackingLoading: false
      }
    case DELETE_PROPERTY_PACKING:
      return {
        ...state,
        propertyPackingLoading: true
      }
    case DELETE_PROPERTY_PACKING_FAIL: {
      error("Cannot delete packing!" + action.payload)
      return {
        ...state,
        propertyPackingLoading: false
      }
    }
    case DELETE_PROPERTY_PACKING_SUCCESS:{
      console.log("action delete success", action.payload)
      return {
        ...state,
        propertiesPacking: state.propertiesPacking.filter(origin => origin.id !== action.payload.id),
        propertyPackingLoading: false,
      }
    }
    default:
      return {...state}
  }
}

export default propertyPacking
import { takeEvery, fork, put, all, call } from "redux-saga/effects"

import {
  CREATE_PROPERTY_PACKING,
  GET_PROPERTIES_PACKING,
  UPDATE_PROPERTY_PACKING,
  DELETE_PROPERTY_PACKING,
} from "./actionTypes"
import {
  getPropertiesPackingFail,
  getPropertiesPackingSuccess,
  createPropertyPackingFail,
  createPropertyPackingSuccess,
  updatePropertyPackingFail,
  updatePropertyPackingSuccess,
  deletePropertyPackingFail,
  deletePropertyPackingSuccess
} from "./actions"
import {
  createPacking,
  updatePacking,
  getPackings,
  deletePacking,
} from "_api/properties"

function* createPropertyPacking({payload: {propertyInfo}}) {
  try {
    const response = yield call(createPacking, propertyInfo)
    console.log("response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createPropertyPackingSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createPropertyPackingFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createPropertyPackingFail(error.message))
    } else {
      yield put(createPropertyPackingFail("Error!!!"))
    }
  }
}

function* updatePropertyPacking({payload: {property}}) {
  try {
    const response = yield call(updatePacking, property.id, property)
    // console.log('response', response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(updatePropertyPackingSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(updatePropertyPackingFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updatePropertyPackingFail(error.message))
    } else {
      yield put(updatePropertyPackingFail("Error!!!"))
    }
  }
}

function* getPropertiesPacking() {
  try{
    const response = yield call(getPackings)
    console.log('response', response)
    // console.log('response.data', response.data)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getPropertiesPackingSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getPropertiesPackingFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getPropertiesPackingFail(error.message))
    } else {
      yield put(getPropertiesPackingFail("Error!!!"))
    }
  }
}

function* deletePropertyPacking({payload: {id}}) {
  try{
    const response = yield call(deletePacking, id)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deletePropertyPackingSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deletePropertyPackingFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deletePropertyPackingFail(error.message))
    } else {
      yield put(deletePropertyPackingFail("Error!!!"))
    }
  }
}

export function* watchPropertyPacking() {
  yield takeEvery(CREATE_PROPERTY_PACKING, createPropertyPacking)
  yield takeEvery(UPDATE_PROPERTY_PACKING, updatePropertyPacking)
  yield takeEvery(GET_PROPERTIES_PACKING, getPropertiesPacking)
  yield takeEvery(DELETE_PROPERTY_PACKING, deletePropertyPacking)
}

function* propetyPackingSaga() {
  yield all([fork(watchPropertyPacking)])
}

export default propetyPackingSaga
import { takeEvery, fork, put, all, call } from "redux-saga/effects"

import {
  CREATE_PROPERTY_SELLBLOCK,
  GET_PROPERTIES_SELLBLOCK,
  UPDATE_PROPERTY_SELLBLOCK,
  DELETE_PROPERTY_SELLBLOCK,
} from "./actionTypes"
import {
  getPropertiesSellBlockFail,
  getPropertiesSellBlockSuccess,
  createPropertySellBlockFail,
  createPropertySellBlockSuccess,
  updatePropertySellBlockFail,
  updatePropertySellBlockSuccess,
  deletePropertySellBlockFail,
  deletePropertySellBlockSuccess
} from "./actions"
import {
  createSellBlock,
  updateSellBlock,
  getSellBlocks,
  deleteSellBlock,
} from "_api/properties"

function* createPropertySellBlock({payload: {propertyInfo}}) {
  try {
    const response = yield call(createSellBlock, propertyInfo)
    console.log("response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createPropertySellBlockSuccess(response.data.data))
    }  else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createPropertySellBlockFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createPropertySellBlockFail(error.message))
    } else {
      yield put(createPropertySellBlockFail("Error!!!"))
    }
  }
}

function* updatePropertySellBlock({payload: {property}}) {
  try {
    const response = yield call(updateSellBlock, property.id, property)
    // console.log('response', response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(updatePropertySellBlockSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(updatePropertySellBlockFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updatePropertySellBlockFail(error.message))
    } else {
      yield put(updatePropertySellBlockFail("Error!!!"))
    }
  }
}

function* getPropertiesSellBlock() {
  try{
    const response = yield call(getSellBlocks)
    console.log('response', response)
    // console.log('response.data', response.data)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getPropertiesSellBlockSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getPropertiesSellBlockFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getPropertiesSellBlockFail(error.message))
    } else {
      yield put(getPropertiesSellBlockFail("Error!!!"))
    }
  }
}

function* deletePropertySellBlock({payload: {id}}) {
  try{
    const response = yield call(deleteSellBlock, id)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deletePropertySellBlockSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deletePropertySellBlockFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deletePropertySellBlockFail(error.message))
    } else {
      yield put(deletePropertySellBlockFail("Error!!!"))
    }
  }
}

export function* watchPropertySellBlock() {
  yield takeEvery(CREATE_PROPERTY_SELLBLOCK, createPropertySellBlock)
  yield takeEvery(UPDATE_PROPERTY_SELLBLOCK, updatePropertySellBlock)
  yield takeEvery(GET_PROPERTIES_SELLBLOCK, getPropertiesSellBlock)
  yield takeEvery(DELETE_PROPERTY_SELLBLOCK, deletePropertySellBlock)
}

function* propetySellBlockSaga() {
  yield all([fork(watchPropertySellBlock)])
}

export default propetySellBlockSaga
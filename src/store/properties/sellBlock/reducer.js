import {
  GET_PROPERTIES_SELLBLOCK,
  GET_PROPERTIES_SELLBLOCK_FAIL,
  GET_PROPERTIES_SELLBLOCK_SUCCESS,
  CREATE_PROPERTY_SELLBLOCK,
  CREATE_PROPERTY_SELLBLOCK_FAIL,
  CREATE_PROPERTY_SELLBLOCK_SUCCESS,
  UPDATE_PROPERTY_SELLBLOCK,
  UPDATE_PROPERTY_SELLBLOCK_FAIL,
  UPDATE_PROPERTY_SELLBLOCK_SUCCESS,
  DELETE_PROPERTY_SELLBLOCK,
  DELETE_PROPERTY_SELLBLOCK_FAIL,
  DELETE_PROPERTY_SELLBLOCK_SUCCESS
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const initialState = {
  propertiesSellBlock: [],
  propertySellBlockLoading: false
}

const propertySellBlock = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES_SELLBLOCK:
      return {
        ...state,
        propertySellBlockLoading: true
      }
    case GET_PROPERTIES_SELLBLOCK_FAIL:{
      error(action.payload)
      return {
        ...state,
        propertySellBlockLoading: false,
        propertiesSellBlock: []
      }
    }
    case GET_PROPERTIES_SELLBLOCK_SUCCESS:{
      success("SellBlocks loaded!!!")
      return {
        ...state,
        propertySellBlockLoading: false,
        propertiesSellBlock: action.payload
      }
    }
    case CREATE_PROPERTY_SELLBLOCK:
      return {
        ...state,
        propertySellBlockLoading: true
      }
    case CREATE_PROPERTY_SELLBLOCK_FAIL:{
      error("Cannot created sell block! " + action.payload)
      return {
        ...state,
        propertySellBlockLoading: false
      }
    }
    case CREATE_PROPERTY_SELLBLOCK_SUCCESS:{
      success("SellBlock created.")
      return {
        ...state,
        propertiesSellBlock: state.propertiesSellBlock.concat(action.payload),
        propertySellBlockLoading: false
      }
    }
    case UPDATE_PROPERTY_SELLBLOCK:
      return {
        ...state,
        propertySellBlockLoading: true
      }
    case UPDATE_PROPERTY_SELLBLOCK_FAIL:
      return {
        ...state,
        propertySellBlockLoading: false
      }
    case UPDATE_PROPERTY_SELLBLOCK_SUCCESS:
      return {
        ...state,
        propertySellBlockLoading: false
      }
    case DELETE_PROPERTY_SELLBLOCK:
      return {
        ...state,
        propertySellBlockLoading: true
      }
    case DELETE_PROPERTY_SELLBLOCK_FAIL: {
      error("Cannot delete sell block!" + action.payload)
      return {
        ...state,
        propertySellBlockLoading: false
      }
    }
    case DELETE_PROPERTY_SELLBLOCK_SUCCESS:{
      success("SellBlock deleted!")
      return {
        ...state,
        propertiesSellBlock: state.propertiesSellBlock.filter(sellBlock => sellBlock.id !== action.payload.id),
        propertySellBlockLoading: false,
      }
    }
    default:
      return {...state}
  }
}

export default propertySellBlock
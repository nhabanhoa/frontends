import {
  CREATE_PROPERTY_SELLBLOCK,
  CREATE_PROPERTY_SELLBLOCK_FAIL,
  CREATE_PROPERTY_SELLBLOCK_SUCCESS,
  UPDATE_PROPERTY_SELLBLOCK,
  UPDATE_PROPERTY_SELLBLOCK_FAIL,
  UPDATE_PROPERTY_SELLBLOCK_SUCCESS,
  GET_PROPERTIES_SELLBLOCK,
  GET_PROPERTIES_SELLBLOCK_FAIL,
  GET_PROPERTIES_SELLBLOCK_SUCCESS,
  DELETE_PROPERTY_SELLBLOCK,
  DELETE_PROPERTY_SELLBLOCK_FAIL,
  DELETE_PROPERTY_SELLBLOCK_SUCCESS
} from "./actionTypes"

export const createPropertySellBlock = (propertyInfo) => ({
  type: CREATE_PROPERTY_SELLBLOCK,
  payload: {propertyInfo}
})

export const createPropertySellBlockFail = error => ({
  type: CREATE_PROPERTY_SELLBLOCK_FAIL,
  payload: error
})

export const createPropertySellBlockSuccess = property => ({
  type: CREATE_PROPERTY_SELLBLOCK_SUCCESS,
  payload: property
})

export const updatePropertySellBlock = property => ({
  type: UPDATE_PROPERTY_SELLBLOCK,
  payload: {property}
})

export const updatePropertySellBlockFail = error => ({
  type: UPDATE_PROPERTY_SELLBLOCK_FAIL,
  payload: error
})

export const updatePropertySellBlockSuccess = property => ({
  type: UPDATE_PROPERTY_SELLBLOCK_SUCCESS,
  payload: property
})

export const getPropertiesSellBlock = () => ({
  type: GET_PROPERTIES_SELLBLOCK,
})

export const getPropertiesSellBlockFail = error => ({
  type: GET_PROPERTIES_SELLBLOCK_FAIL,
  payload: error
})

export const getPropertiesSellBlockSuccess = properties => ({
  type: GET_PROPERTIES_SELLBLOCK_SUCCESS,
  payload: properties
})

export const deletePropertySellBlock = (id) => ({
  type: DELETE_PROPERTY_SELLBLOCK,
  payload: {id}
})

export const deletePropertySellBlockFail = error => ({
  type: DELETE_PROPERTY_SELLBLOCK_FAIL,
  payload: error
})

export const deletePropertySellBlockSuccess = info => ({
  type: DELETE_PROPERTY_SELLBLOCK_SUCCESS,
  payload: info
})
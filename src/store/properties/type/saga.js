import { takeEvery, fork, put, all, call } from "redux-saga/effects"

import {
  CREATE_PROPERTY_TYPE,
  GET_PROPERTIES_TYPE,
  UPDATE_PROPERTY_TYPE,
  DELETE_PROPERTY_TYPE,
} from "./actionTypes"
import {
  getPropertiesTypeFail,
  getPropertiesTypeSuccess,
  createPropertyTypeFail,
  createPropertyTypeSuccess,
  updatePropertyTypeFail,
  updatePropertyTypeSuccess,
  deletePropertyTypeFail,
  deletePropertyTypeSuccess
} from "./actions"
import {
  createType,
  updateType,
  getTypes,
  deleteType,
} from "_api/properties"

function* createPropertyType({payload: {propertyInfo}}) {
  try {
    const response = yield call(createType, propertyInfo)
    console.log("response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createPropertyTypeSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createPropertyTypeFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createPropertyTypeFail(error.message))
    } else {
      yield put(createPropertyTypeFail("Error!!!"))
    }
  }
}

function* updatePropertyType({payload: {property}}) {
  try {
    const response = yield call(updateType, property.id, property)
    // console.log('response', response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(updatePropertyTypeSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(updatePropertyTypeFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updatePropertyTypeFail(error.message))
    } else {
      yield put(updatePropertyTypeFail("Error!!!"))
    }
  }
}

function* getPropertiesType() {
  try{
    const response = yield call(getTypes)
    console.log('response', response)
    // console.log('response.data', response.data)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getPropertiesTypeSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getPropertiesTypeFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getPropertiesTypeFail(error.message))
    } else {
      yield put(getPropertiesTypeFail("Error!!!"))
    }
  }
}

function* deletePropertyType({payload: {id}}) {
  try{
    const response = yield call(deleteType, id)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deletePropertyTypeSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deletePropertyTypeFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deletePropertyTypeFail(error.message))
    } else {
      yield put(deletePropertyTypeFail("Error!!!"))
    }
  }
}

export function* watchPropertyType() {
  yield takeEvery(CREATE_PROPERTY_TYPE, createPropertyType)
  yield takeEvery(UPDATE_PROPERTY_TYPE, updatePropertyType)
  yield takeEvery(GET_PROPERTIES_TYPE, getPropertiesType)
  yield takeEvery(DELETE_PROPERTY_TYPE, deletePropertyType)
}

function* propetyTypeSaga() {
  yield all([fork(watchPropertyType)])
}

export default propetyTypeSaga
import {
  CREATE_PROPERTY_TYPE,
  CREATE_PROPERTY_TYPE_FAIL,
  CREATE_PROPERTY_TYPE_SUCCESS,
  UPDATE_PROPERTY_TYPE,
  UPDATE_PROPERTY_TYPE_FAIL,
  UPDATE_PROPERTY_TYPE_SUCCESS,
  GET_PROPERTIES_TYPE,
  GET_PROPERTIES_TYPE_FAIL,
  GET_PROPERTIES_TYPE_SUCCESS,
  DELETE_PROPERTY_TYPE,
  DELETE_PROPERTY_TYPE_FAIL,
  DELETE_PROPERTY_TYPE_SUCCESS
} from "./actionTypes"

export const createPropertyType = (propertyInfo) => ({
  type: CREATE_PROPERTY_TYPE,
  payload: {propertyInfo}
})

export const createPropertyTypeFail = error => ({
  type: CREATE_PROPERTY_TYPE_FAIL,
  payload: error
})

export const createPropertyTypeSuccess = property => ({
  type: CREATE_PROPERTY_TYPE_SUCCESS,
  payload: property
})

export const updatePropertyType = property => ({
  type: UPDATE_PROPERTY_TYPE,
  payload: {property}
})

export const updatePropertyTypeFail = error => ({
  type: UPDATE_PROPERTY_TYPE_FAIL,
  payload: error
})

export const updatePropertyTypeSuccess = property => ({
  type: UPDATE_PROPERTY_TYPE_SUCCESS,
  payload: property
})

export const getPropertiesType = () => ({
  type: GET_PROPERTIES_TYPE,
})

export const getPropertiesTypeFail = error => ({
  type: GET_PROPERTIES_TYPE_FAIL,
  payload: error
})

export const getPropertiesTypeSuccess = properties => ({
  type: GET_PROPERTIES_TYPE_SUCCESS,
  payload: properties
})

export const deletePropertyType = (id) => ({
  type: DELETE_PROPERTY_TYPE,
  payload: {id}
})

export const deletePropertyTypeFail = error => ({
  type: DELETE_PROPERTY_TYPE_FAIL,
  payload: error
})

export const deletePropertyTypeSuccess = info => ({
  type: DELETE_PROPERTY_TYPE_SUCCESS,
  payload: info
})
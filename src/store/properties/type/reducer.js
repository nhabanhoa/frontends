import {
  GET_PROPERTIES_TYPE,
  GET_PROPERTIES_TYPE_FAIL,
  GET_PROPERTIES_TYPE_SUCCESS,
  CREATE_PROPERTY_TYPE,
  CREATE_PROPERTY_TYPE_FAIL,
  CREATE_PROPERTY_TYPE_SUCCESS,
  UPDATE_PROPERTY_TYPE,
  UPDATE_PROPERTY_TYPE_FAIL,
  UPDATE_PROPERTY_TYPE_SUCCESS,
  DELETE_PROPERTY_TYPE,
  DELETE_PROPERTY_TYPE_FAIL,
  DELETE_PROPERTY_TYPE_SUCCESS
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const initialState = {
  propertiesType: [],
  propertyTypeLoading: false
}

const propertyType = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES_TYPE:
      return {
        ...state,
        propertyTypeLoading: true
      }
    case GET_PROPERTIES_TYPE_FAIL:{
      error(action.payload)
      return {
        ...state,
        propertyTypeLoading: false,
        propertiesType: []
      }
    }
    case GET_PROPERTIES_TYPE_SUCCESS:{
      success("Types loaded!!!")
      return {
        ...state,
        propertyTypeLoading: false,
        propertiesType: action.payload
      }
    }
    case CREATE_PROPERTY_TYPE:
      return {
        ...state,
        propertyTypeLoading: true
      }
    case CREATE_PROPERTY_TYPE_FAIL:{
      error("Cannot created type! " + action.payload)
      return {
        ...state,
        propertyTypeLoading: false
      }
    }
    case CREATE_PROPERTY_TYPE_SUCCESS:{
      success("Type " + action.payload.name + " created.")
      return {
        ...state,
        propertiesType: state.propertiesType.concat(action.payload),
        propertyTypeLoading: false
      }
    }
    case UPDATE_PROPERTY_TYPE:
      return {
        ...state,
        propertyTypeLoading: true
      }
    case UPDATE_PROPERTY_TYPE_FAIL:
      return {
        ...state,
        propertyTypeLoading: false
      }
    case UPDATE_PROPERTY_TYPE_SUCCESS:
      return {
        ...state,
        propertyTypeLoading: false
      }
    case DELETE_PROPERTY_TYPE:
      return {
        ...state,
        propertyTypeLoading: true
      }
    case DELETE_PROPERTY_TYPE_FAIL: {
      error("Cannot delete type! " + action.payload)
      return {
        ...state,
        propertyTypeLoading: false
      }
    }
    case DELETE_PROPERTY_TYPE_SUCCESS:{
      success("Type " + action.payload.name + " deleted!")
      return {
        ...state,
        propertiesType: state.propertiesType.filter(type => type.id !== action.payload.id),
        propertyTypeLoading: false,
      }
    }
    default:
      return {...state}
  }
}

export default propertyType
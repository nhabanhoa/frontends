import { takeEvery, fork, put, all, call } from "redux-saga/effects"

import {
  CREATE_PROPERTY_ORIGIN,
  GET_PROPERTIES_ORIGIN,
  UPDATE_PROPERTY_ORIGIN,
  DELETE_PROPERTY_ORIGIN,
} from "./actionTypes"
import {
  getPropertiesOriginFail,
  getPropertiesOriginSuccess,
  createPropertyOriginFail,
  createPropertyOriginSuccess,
  updatePropertyOriginFail,
  updatePropertyOriginSuccess,
  deletePropertyOriginFail,
  deletePropertyOriginSuccess
} from "./actions"
import {
  createOrigin,
  updateOrigin,
  getOrigins,
  deleteOrigin,
} from "_api/properties"

function* createPropertyOrigin({payload: {propertyInfo}}) {
  try {
    const response = yield call(createOrigin, propertyInfo)
    console.log("response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createPropertyOriginSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createPropertyOriginFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createPropertyOriginFail(error.message))
    } else {
      yield put(createPropertyOriginFail("Error!!!"))
    }
  }
}

function* updatePropertyOrigin({payload: {property}}) {
  try {
    const response = yield call(updateOrigin, property.id, property)
    // console.log('response', response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(updatePropertyOriginSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(updatePropertyOriginFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updatePropertyOriginFail(error.message))
    } else {
      yield put(updatePropertyOriginFail("Error!!!"))
    }
  }
}

function* getPropertiesOrigin() {
  try{
    const response = yield call(getOrigins)
    console.log('response', response)
    // console.log('response.data', response.data)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getPropertiesOriginSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getPropertiesOriginFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getPropertiesOriginFail(error.message))
    } else {
      yield put(getPropertiesOriginFail("Error!!!"))
    }
  }
}

function* deletePropertyOrigin({payload: {id}}) {
  try{
    const response = yield call(deleteOrigin, id)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deletePropertyOriginSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deletePropertyOriginFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deletePropertyOriginFail(error.message))
    } else {
      yield put(deletePropertyOriginFail("Error!!!"))
    }
  }
}

export function* watchPropertyOrigin() {
  yield takeEvery(CREATE_PROPERTY_ORIGIN, createPropertyOrigin)
  yield takeEvery(UPDATE_PROPERTY_ORIGIN, updatePropertyOrigin)
  yield takeEvery(GET_PROPERTIES_ORIGIN, getPropertiesOrigin)
  yield takeEvery(DELETE_PROPERTY_ORIGIN, deletePropertyOrigin)
}

function* propetyOriginSaga() {
  yield all([fork(watchPropertyOrigin)])
}

export default propetyOriginSaga
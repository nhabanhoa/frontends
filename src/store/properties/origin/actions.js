import {
  CREATE_PROPERTY_ORIGIN,
  CREATE_PROPERTY_ORIGIN_FAIL,
  CREATE_PROPERTY_ORIGIN_SUCCESS,
  UPDATE_PROPERTY_ORIGIN,
  UPDATE_PROPERTY_ORIGIN_FAIL,
  UPDATE_PROPERTY_ORIGIN_SUCCESS,
  GET_PROPERTIES_ORIGIN,
  GET_PROPERTIES_ORIGIN_FAIL,
  GET_PROPERTIES_ORIGIN_SUCCESS,
  DELETE_PROPERTY_ORIGIN,
  DELETE_PROPERTY_ORIGIN_FAIL,
  DELETE_PROPERTY_ORIGIN_SUCCESS
} from "./actionTypes"

export const createPropertyOrigin = (propertyInfo) => ({
  type: CREATE_PROPERTY_ORIGIN,
  payload: {propertyInfo}
})

export const createPropertyOriginFail = error => ({
  type: CREATE_PROPERTY_ORIGIN_FAIL,
  payload: error
})

export const createPropertyOriginSuccess = property => ({
  type: CREATE_PROPERTY_ORIGIN_SUCCESS,
  payload: property
})

export const updatePropertyOrigin = property => ({
  type: UPDATE_PROPERTY_ORIGIN,
  payload: {property}
})

export const updatePropertyOriginFail = error => ({
  type: UPDATE_PROPERTY_ORIGIN_FAIL,
  payload: error
})

export const updatePropertyOriginSuccess = property => ({
  type: UPDATE_PROPERTY_ORIGIN_SUCCESS,
  payload: property
})

export const getPropertiesOrigin = () => ({
  type: GET_PROPERTIES_ORIGIN,
})

export const getPropertiesOriginFail = error => ({
  type: GET_PROPERTIES_ORIGIN_FAIL,
  payload: error
})

export const getPropertiesOriginSuccess = properties => ({
  type: GET_PROPERTIES_ORIGIN_SUCCESS,
  payload: properties
})

export const deletePropertyOrigin = (id) => ({
  type: DELETE_PROPERTY_ORIGIN,
  payload: {id}
})

export const deletePropertyOriginFail = error => ({
  type: DELETE_PROPERTY_ORIGIN_FAIL,
  payload: error
})

export const deletePropertyOriginSuccess = info => ({
  type: DELETE_PROPERTY_ORIGIN_SUCCESS,
  payload: info
})
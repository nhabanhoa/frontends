import {
  GET_PROPERTIES_ORIGIN,
  GET_PROPERTIES_ORIGIN_FAIL,
  GET_PROPERTIES_ORIGIN_SUCCESS,
  CREATE_PROPERTY_ORIGIN,
  CREATE_PROPERTY_ORIGIN_FAIL,
  CREATE_PROPERTY_ORIGIN_SUCCESS,
  UPDATE_PROPERTY_ORIGIN,
  UPDATE_PROPERTY_ORIGIN_FAIL,
  UPDATE_PROPERTY_ORIGIN_SUCCESS,
  DELETE_PROPERTY_ORIGIN,
  DELETE_PROPERTY_ORIGIN_FAIL,
  DELETE_PROPERTY_ORIGIN_SUCCESS
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const initialState = {
  propertiesOrigin: [],
  propertyOriginLoading: false
}

const propertyOrigin = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES_ORIGIN:
      return {
        ...state,
        propertyOriginLoading: true
      }
    case GET_PROPERTIES_ORIGIN_FAIL:{
      error(action.payload)
      return {
        ...state,
        propertyOriginLoading: false,
        propertiesOrigin: []
      }
    }
    case GET_PROPERTIES_ORIGIN_SUCCESS:{
      success("Origins loaded!!!")
      return {
        ...state,
        propertyOriginLoading: false,
        propertiesOrigin: action.payload
      }
    }
    case CREATE_PROPERTY_ORIGIN:
      return {
        ...state,
        propertyOriginLoading: true
      }
    case CREATE_PROPERTY_ORIGIN_FAIL:{
      error("Cannot created origin! " + action.payload)
      return {
        ...state,
        propertyOriginLoading: false
      }
    }
    case CREATE_PROPERTY_ORIGIN_SUCCESS:{
      success("Origin " + action.payload.name + " created.")
      return {
        ...state,
        propertiesOrigin: state.propertiesOrigin.concat(action.payload),
        propertyOriginLoading: false
      }
    }
    case UPDATE_PROPERTY_ORIGIN:
      return {
        ...state,
        propertyOriginLoading: true
      }
    case UPDATE_PROPERTY_ORIGIN_FAIL:
      return {
        ...state,
        propertyOriginLoading: false
      }
    case UPDATE_PROPERTY_ORIGIN_SUCCESS:
      return {
        ...state,
        propertyOriginLoading: false
      }
    case DELETE_PROPERTY_ORIGIN:
      return {
        ...state,
        propertyOriginLoading: true
      }
    case DELETE_PROPERTY_ORIGIN_FAIL: {
      error("Cannot delete origin! " + action.payload)
      return {
        ...state,
        propertyOriginLoading: false
      }
    }
    case DELETE_PROPERTY_ORIGIN_SUCCESS:{
      success("Origin " + action.payload.name + " deleted!")
      return {
        ...state,
        propertiesOrigin: state.propertiesOrigin.filter(origin => origin.id !== action.payload.id),
        propertyOriginLoading: false,
      }
    }
    default:
      return {...state}
  }
}

export default propertyOrigin
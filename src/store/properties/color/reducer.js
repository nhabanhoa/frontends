import {
  GET_PROPERTIES_COLOR,
  GET_PROPERTIES_COLOR_FAIL,
  GET_PROPERTIES_COLOR_SUCCESS,
  CREATE_PROPERTY_COLOR,
  CREATE_PROPERTY_COLOR_FAIL,
  CREATE_PROPERTY_COLOR_SUCCESS,
  UPDATE_PROPERTY_COLOR,
  UPDATE_PROPERTY_COLOR_FAIL,
  UPDATE_PROPERTY_COLOR_SUCCESS,
  DELETE_PROPERTY_COLOR,
  DELETE_PROPERTY_COLOR_FAIL,
  DELETE_PROPERTY_COLOR_SUCCESS,
  CLEAR_COLOR
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const initialState = {
  propertiesColor: [],
  propertyColorLoading: false
}

const propertyColor = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROPERTIES_COLOR:
      return {
        ...state,
        propertyColorLoading: true
      }
    case GET_PROPERTIES_COLOR_FAIL: {
      error(action.payload)
      return {
        ...state,
        propertyColorLoading: false,
        propertiesColor: []
      }
    }

    case GET_PROPERTIES_COLOR_SUCCESS: {
      success("Colors loaded!!!")
      return {
        ...state,
        propertyColorLoading: false,
        propertiesColor: action.payload
      }
    }

    case CREATE_PROPERTY_COLOR:
      return {
        ...state,
        propertyColorLoading: true
      }

    case CREATE_PROPERTY_COLOR_FAIL: {
      error("Cannot created color! " + action.payload)
      return {
        ...state,
        propertyColorLoading: false
      }
    }

    case CREATE_PROPERTY_COLOR_SUCCESS: {
      success("Color " + action.payload.name + " created.")
      return {
        ...state,
        propertiesColor: state.propertiesColor.concat(action.payload),
        propertyColorLoading: false
      }
    }

    case UPDATE_PROPERTY_COLOR:
      return {
        ...state,
        propertyColorLoading: true
      }
    case UPDATE_PROPERTY_COLOR_FAIL:
      return {
        ...state,
        propertyColorLoading: false
      }
    case UPDATE_PROPERTY_COLOR_SUCCESS:
      return {
        ...state,
        propertyColorLoading: false
      }
    case DELETE_PROPERTY_COLOR:
      return {
        ...state,
        propertyColorLoading: true
      }
    case DELETE_PROPERTY_COLOR_FAIL: {
      error("Cannot delete color! " + action.payload)
      return {
        ...state,
        propertyColorLoading: false
      }
    }

    case DELETE_PROPERTY_COLOR_SUCCESS: {
      success("Color " + action.payload.name + " deleted!")
      return {
        ...state,
        propertiesColor: state.propertiesColor.filter(color => color.id !== action.payload.id),
        propertyColorLoading: false,
      }
    }

    case CLEAR_COLOR:
      return {
        ...state,
      }
      default:
      return {...state}
  }
}

export default propertyColor
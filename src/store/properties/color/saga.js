import { takeEvery, fork, put, all, call } from "redux-saga/effects"

import {
  CREATE_PROPERTY_COLOR,
  GET_PROPERTIES_COLOR,
  UPDATE_PROPERTY_COLOR,
  DELETE_PROPERTY_COLOR,
} from "./actionTypes"
import {
  getPropertiesColorFail,
  getPropertiesColorSuccess,
  createPropertyColorFail,
  createPropertyColorSuccess,
  updatePropertyColorFail,
  updatePropertyColorSuccess,
  deletePropertyColorFail,
  deletePropertyColorSuccess
} from "./actions"
import {
  createColor,
  updateColor,
  getColors,
  deleteColor,
} from "_api/properties"

function* createPropertyColor({payload: {propertyInfo}}) {
  try {
    const response = yield call(createColor, propertyInfo)
    // console.log("response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createPropertyColorSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createPropertyColorFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createPropertyColorFail(error.message))
    } else {
      yield put(createPropertyColorFail("Error!!!"))
    }
  }
}

function* updatePropertyColor({payload: {property}}) {
  try {
    const response = yield call(updateColor, property.id, property)
    // console.log('response', response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(updatePropertyColorSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
    yield put(updatePropertyColorSuccess(response))
  } catch (error) {
    if (typeof error === "string") {
      yield put(updatePropertyColorFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updatePropertyColorFail(error.message))
    } else {
      yield put(updatePropertyColorFail("Error!!!"))
    }
  }
}

function* getPropertiesColor() {
  try{
    const response = yield call(getColors)
    console.log('response', response)
    // console.log('response.data', response.data)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getPropertiesColorSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getPropertiesColorFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getPropertiesColorFail(error.message))
    } else {
      yield put(getPropertiesColorFail("Error!!!"))
    }
  }
}

function* deletePropertyColor({payload: {id}}) {
  try{
    const response = yield call(deleteColor, id)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deletePropertyColorSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deletePropertyColorFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deletePropertyColorFail(error.message))
    } else {
      yield put(deletePropertyColorFail("Error!!!"))
    }
  }
}

export function* watchPropertyColor() {
  yield takeEvery(CREATE_PROPERTY_COLOR, createPropertyColor)
  yield takeEvery(UPDATE_PROPERTY_COLOR, updatePropertyColor)
  yield takeEvery(GET_PROPERTIES_COLOR, getPropertiesColor)
  yield takeEvery(DELETE_PROPERTY_COLOR, deletePropertyColor)
}

function* propetyColorSaga() {
  yield all([fork(watchPropertyColor)])
}

export default propetyColorSaga
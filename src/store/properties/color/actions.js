import {
  CREATE_PROPERTY_COLOR,
  CREATE_PROPERTY_COLOR_FAIL,
  CREATE_PROPERTY_COLOR_SUCCESS,
  UPDATE_PROPERTY_COLOR,
  UPDATE_PROPERTY_COLOR_FAIL,
  UPDATE_PROPERTY_COLOR_SUCCESS,
  GET_PROPERTIES_COLOR,
  GET_PROPERTIES_COLOR_FAIL,
  GET_PROPERTIES_COLOR_SUCCESS,
  DELETE_PROPERTY_COLOR,
  DELETE_PROPERTY_COLOR_FAIL,
  DELETE_PROPERTY_COLOR_SUCCESS,
  CLEAR_COLOR
} from "./actionTypes"

export const createPropertyColor = (propertyInfo) => ({
  type: CREATE_PROPERTY_COLOR,
  payload: {propertyInfo}
})

export const createPropertyColorFail = error => ({
  type: CREATE_PROPERTY_COLOR_FAIL,
  payload: error
})

export const createPropertyColorSuccess = property => ({
  type: CREATE_PROPERTY_COLOR_SUCCESS,
  payload: property
})

export const updatePropertyColor = property => ({
  type: UPDATE_PROPERTY_COLOR,
  payload: {property}
})

export const updatePropertyColorFail = error => ({
  type: UPDATE_PROPERTY_COLOR_FAIL,
  payload: error
})

export const updatePropertyColorSuccess = property => ({
  type: UPDATE_PROPERTY_COLOR_SUCCESS,
  payload: property
})

export const getPropertiesColor = () => ({
  type: GET_PROPERTIES_COLOR,
})

export const getPropertiesColorFail = error => ({
  type: GET_PROPERTIES_COLOR_FAIL,
  payload: error
})

export const getPropertiesColorSuccess = properties => ({
  type: GET_PROPERTIES_COLOR_SUCCESS,
  payload: properties
})

export const deletePropertyColor = (id) => ({
  type: DELETE_PROPERTY_COLOR,
  payload: {id}
})

export const deletePropertyColorFail = error => ({
  type: DELETE_PROPERTY_COLOR_FAIL,
  payload: error
})

export const deletePropertyColorSuccess = info => ({
  type: DELETE_PROPERTY_COLOR_SUCCESS,
  payload: info
})

export const clearColor = () => ({
  type: CLEAR_COLOR,
  payload: {}
})
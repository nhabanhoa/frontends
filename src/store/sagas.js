import { all, fork } from "redux-saga/effects"

//public
import AccountSaga from "./auth/register/saga"
import AuthSaga from "./auth/login/saga"
import ForgetSaga from "./auth/forgetpwd/saga"
import ProfileSaga from "./profile/saga"
import LayoutSaga from "./layout/saga"
import ProductSaga from "./product/saga"
import S3Saga from "./s3/saga"
import PropertyColor from "./properties/color/saga"
import PropertyOrigin from "./properties/origin/saga"
import PropertyPacking from "./properties/packing/saga"
import PropertySellBlock from "./properties/sellBlock/saga"
import PropertyType from "./properties/type/saga"
// import PropertySaga from "./properties/saga"
// import ecommerceSaga from "./e-commerce/saga"
// import calendarSaga from "./calendar/saga"
// import chatSaga from "./chat/saga"
// import cryptoSaga from "./crypto/saga"
// import invoiceSaga from "./invoices/saga"
// import projectsSaga from "./projects/saga"
// import tasksSaga from "./tasks/saga"
// import contactsSaga from "./contacts/saga"

export default function* rootSaga() {
  yield all([
    //public
    AccountSaga(),
    fork(AuthSaga),
    ForgetSaga(),
    ProfileSaga(),
    LayoutSaga(),
    ProductSaga(),
    S3Saga(),
    PropertyColor(),
    PropertyOrigin(),
    PropertyPacking(),
    PropertySellBlock(),
    PropertyType()
    // PropertySaga(),
    // fork(ecommerceSaga),
    // fork(calendarSaga),
    // fork(chatSaga),
    // fork(cryptoSaga),
    // fork(invoiceSaga),
    // fork(projectsSaga),
    // fork(tasksSaga),
    // fork(contactsSaga),
  ])
}

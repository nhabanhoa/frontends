import { call, put, takeEvery, takeLeading } from "redux-saga/effects"

// Login Redux States
import { LOGIN_USER, LOGOUT_USER } from "./actionTypes"
import { apiError, loginSuccess, logoutUserSuccess } from "./actions"

import {amplifySignUp} from "_utils/amplify/function"

function* loginUser({ payload: { user, history } }) {
  try {
    // const response = yield call(postLogin, {
    //   email: user.email,
    //   password: user.password,
    // })
    //
    // localStorage.setItem("authUser", JSON.stringify(response))
    // yield put(loginSuccess(response))
    // history.push("/dashboard")

    const response = yield call(amplifySignUp, user)
    // console.log("response", response)

  } catch (error) {
    // console.log("error", error)
    yield put(apiError(error))
  }
}

function* logoutUser({ payload: { history } }) {
  try {
    localStorage.removeItem("authUser")
    history.push("/login")
  } catch (error) {
    yield put(apiError(error))
  }
}

function* authSaga() {
  yield takeLeading(LOGIN_USER, loginUser)
  yield takeLeading(LOGOUT_USER, logoutUser)
}

export default authSaga

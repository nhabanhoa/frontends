import { takeEvery, fork, put, all, call } from "redux-saga/effects"

//Account Redux states
import { REGISTER_USER } from "./actionTypes"
import { registerUserSuccessful, registerUserFailed } from "./actions"

import {amplifySignUp} from "_utils/amplify/function"

// Is user register successfull then direct plot user in redux.
function* registerUser({payload: {userInfo}}) {
  // console.log("userInfo", userInfo)

  try {
    // console.log("userInfo", userInfo)

    const response = yield call(amplifySignUp, userInfo)

    // console.log("response", response)

    yield put(registerUserSuccessful(response))
  } catch (error) {
    // console.log("error", error)
    yield put(registerUserFailed(error.message))
  }
}

export function* watchUserRegister() {
  yield takeEvery(REGISTER_USER, registerUser)
}

function* accountSaga() {
  yield all([fork(watchUserRegister)])
}

export default accountSaga

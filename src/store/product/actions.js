import {
  CREATE_PRODUCT,
  CREATE_PRODUCT_SUCCESS,
  CREATE_PRODUCT_FAIL,
  UPDATE_PRODUCT,
  UPDATE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_FAIL,
  GET_PRODUCTS,
  GET_PRODUCTS_FAIL,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_DETAIL,
  GET_PRODUCT_DETAIL_FAIL,
  GET_PRODUCT_DETAIL_SUCCESS,
  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
} from "./actionTypes"

export const getProducts = body => ({
  type: GET_PRODUCTS,
  payload: {body}
})

export const getProductsSuccess = products => ({
  type: GET_PRODUCTS_SUCCESS,
  payload: products,
})

export const getProductsFail = error => ({
  type: GET_PRODUCTS_FAIL,
  payload: error,
})

export const getProductDetail = productId => ({
  type: GET_PRODUCT_DETAIL,
  payload: {productId},
})

export const getProductDetailSuccess = products => ({
  type: GET_PRODUCT_DETAIL_SUCCESS,
  payload: products,
})

export const getProductDetailFail = error => ({
  type: GET_PRODUCT_DETAIL_FAIL,
  payload: error,
})

export const createProduct = info => ({
  type: CREATE_PRODUCT,
  payload: {info}
})

export const createProductFail = error => ({
  type: CREATE_PRODUCT_FAIL,
  payload: error
})

export const createProductSuccess = product => ({
  type: CREATE_PRODUCT_SUCCESS,
  payload: product
})

export const updateProduct = info => ({
  type: UPDATE_PRODUCT,
  payload: {info}
})

export const updateProductFail = error => ({
  type: UPDATE_PRODUCT_FAIL,
  payload: error
})

export const updateProductSuccess = product => ({
  type: UPDATE_PRODUCT_SUCCESS,
  payload: product
})

export const deleteProduct = productId => ({
  type: DELETE_PRODUCT,
  payload: {productId}
})

export const deleteProductFail = error => ({
  type: DELETE_PRODUCT_FAIL,
  payload: error
})

export const deleteProductSuccess = state => ({
  type: DELETE_PRODUCT_SUCCESS,
  payload: state
})

import {
  GET_PRODUCTS,
  GET_PRODUCTS_FAIL,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_DETAIL,
  GET_PRODUCT_DETAIL_SUCCESS,
  GET_PRODUCT_DETAIL_FAIL,
  CREATE_PRODUCT,
  CREATE_PRODUCT_FAIL,
  CREATE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT,
  UPDATE_PRODUCT_FAIL,
  UPDATE_PRODUCT_SUCCESS,
  DELETE_PRODUCT,
  DELETE_PRODUCT_FAIL,
  DELETE_PRODUCT_SUCCESS
} from "./actionTypes"

import {error, warn, info, success} from '_utils/custom-toastify'

const INIT_STATE = {
  products: null,
  productLoading: false
}

const Product = (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return {
        ...state,
        productLoading: true,
      }
    case GET_PRODUCTS_SUCCESS: {
      success("Products loaded.")
      return {
        ...state,
        products: action.payload,
        productLoading: false
      }
    }

    case GET_PRODUCTS_FAIL: {
      error("Cannot get products! " + action.payload)
      return {
        ...state,
        productLoading: false
      }
    }

    case GET_PRODUCT_DETAIL:
      return {
        ...state,
        productLoading: true,
      }

    case GET_PRODUCT_DETAIL_SUCCESS: {
      success("Product " + action.payload.name + " loaded.")
      return {
        ...state,
        products: action.payload,
        productLoading: false,
      }
    }

    case GET_PRODUCT_DETAIL_FAIL: {
      error("Product load fail!!! " + action.payload)
      return {
        ...state,
        productLoading: false,
        products: null
      }
    }

    case CREATE_PRODUCT: return {
      ...state,
      productLoading: true,
    }

    case CREATE_PRODUCT_FAIL: {
      error("Fail to create product!!! " + action.payload)
      return {
        ...state,
        productLoading: false
      }
    }

    case CREATE_PRODUCT_SUCCESS: {
      success("Product " + action.payload + " created.")
      return {
        ...state,
        productLoading: false,
      }
    }

    case UPDATE_PRODUCT_FAIL: {
      error("Cannot update product!!!")
      return {
        ...state,
        productError: action.payload,
        productLoading: false
      }
    }

    case UPDATE_PRODUCT_SUCCESS:
      return {
        ...state,
        products: action.payload,
        productLoading: false,
      }

    case DELETE_PRODUCT:
      return {
        ...state,
        productError: null,
        productLoading: true
      }

    case DELETE_PRODUCT_FAIL:
      return {
        ...state,
        productError: action.payload,
        productLoading: false
      }

    case DELETE_PRODUCT_SUCCESS:
      success("Product " + action.payload.name + " deleted!")
      return {
        ...state,
        state: state.products.filter(prod => prod.id !== action.payload.id),
        productLoading: false
      }

    default:
      return state
  }
}

export default Product

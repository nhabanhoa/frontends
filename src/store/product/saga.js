import { call, put, takeEvery, takeLatest } from "redux-saga/effects"

// Ecommerce Redux States
import {
  CREATE_PRODUCT, DELETE_PRODUCT,
  GET_PRODUCT_DETAIL,
  GET_PRODUCTS, UPDATE_PRODUCT,
} from "./actionTypes"

import {
  createProductFail,
  createProductSuccess,
  deleteProductFail,
  deleteProductSuccess,
  getProductDetailFail,
  getProductDetailSuccess,
  getProductsFail,
  getProductsSuccess,
  updateProductFail,
  updateProductSuccess,
} from "./actions"

//Helpers
import {
  searchProducts,
  getProductDetail,
  createProduct,
  deleteProduct,
  updateProduct
} from "_api/product"

function* fetchProducts({payload: {body}}) {
  console.log("fetchProducts")
  try {
    const response = yield call(searchProducts,body)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getProductsSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getProductsFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getProductsFail(error.message))
    } else {
      yield put(getProductsFail("Error!!!"))
    }
  }
}

function* fetchProductDetail({payload: {productId}}) {
  try {
    const response = yield call(getProductDetail, productId)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(getProductDetailSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(getProductDetailFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(getProductDetailFail(error.message))
    } else {
      yield put(getProductDetailFail("Error!!!"))
    }
    
  }
}

function* addProduct({payload: {info}}) {
  try {
    const response = yield call(createProduct, info)
    console.log("create product response", response)
    if (response.status === 200 && response.data.statusCode === 200){
      yield put(createProductSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(createProductFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(createProductFail(error.message))
    } else {
      yield put(createProductFail("Error!!!"))
    }
  }
}

function* editProduct({payload: {info}}) {
  try {
    const response = yield call(updateProduct, info)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(updateProductSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }    
  } catch (error) {
    if (typeof error === "string") {
      yield put(updateProductFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(updateProductFail(error.message))
    } else {
      yield put(updateProductFail("Error!!!"))
    }
  }
}

function* removeProduct({payload: {productId}}) {
  try {
    const response = yield call(deleteProduct, productId)
    if (response.status === 200 && response.data.statusCode === 200){
      console.log("deleted color", response.data.data)
      yield put(deleteProductSuccess(response.data.data))
    } else if (response.status === 200) {
      // console.log("response", response)
      throw response.data.messageString
    } else {
      throw  response
    }
  } catch (error) {
    if (typeof error === "string") {
      yield put(deleteProductFail(error))
    } else if (error.message && typeof error.message === "string") {
      yield put(deleteProductFail(error.message))
    } else {
      yield put(deleteProductFail("Error!!!"))
    }

  }
}

function* ProductSaga() {
  yield takeEvery(GET_PRODUCTS, fetchProducts)
  yield takeEvery(GET_PRODUCT_DETAIL, fetchProductDetail)
  yield takeLatest(CREATE_PRODUCT, addProduct)
  yield takeLatest(UPDATE_PRODUCT, editProduct)
  yield takeLatest(DELETE_PRODUCT, removeProduct)
}

export default ProductSaga

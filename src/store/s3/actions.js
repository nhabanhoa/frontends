import {
  UPLOAD_IMAGE_S3,
  UPLOAD_IMAGE_S3_FAIL,
  UPLOAD_IMAGE_S3_SUCCESS,
  // GET_IMAGE_S3,
  // GET_IMAGE_S3_FAIL,
  // GET_IMAGE_S3_SUCCESS
} from "./actionTypes"

export const uploadImageS3 = files => {
  // console.log("dispatch action")
  return ({
    type: UPLOAD_IMAGE_S3,
    payload: {files}
  })
}

export const uploadImageS3Fail = error => ({
  type: UPLOAD_IMAGE_S3_FAIL,
  payload: error
})

export const uploadImageS3Success = s3path => ({
  type: UPLOAD_IMAGE_S3_SUCCESS,
  payload: s3path
})

// export const getImageS3 = files => ({
//   type: GET_IMAGE_S3,
//   payload: {files}
// })
//
// export const getImageS3Fail = error => ({
//   type: GET_IMAGE_S3_FAIL,
//   payload: error
// })
//
// export const getImageS3Success = s3path => ({
//   type: GET_IMAGE_S3_SUCCESS,
//   payload: s3path
// })
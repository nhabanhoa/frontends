import {
  UPLOAD_IMAGE_S3,
  UPLOAD_IMAGE_S3_FAIL,
  UPLOAD_IMAGE_S3_SUCCESS,
  // GET_IMAGE_S3,
  // GET_IMAGE_S3_FAIL,
  // GET_IMAGE_S3_SUCCESS
} from "./actionTypes"

const initialState = {
  s3Error: "",
  s3path: "",
  s3Loading: false,
  // file: null,
  // files: null
}

const s3 = (state = initialState, action) => {
  // console.log("switch action", action.type)
  switch (action.type) {
    case UPLOAD_IMAGE_S3:
      state = {
        ...state,
        s3path: "",
        s3Error: "",
        s3Loading: true
      }
      break
    case UPLOAD_IMAGE_S3_FAIL:
      state = {
        ...state,
        s3path: "",
        s3Error: action.payload,
        s3Loading: false
      }
      break
    case UPLOAD_IMAGE_S3_SUCCESS:
      state = {
        ...state,
        s3path: action.payload,
        s3Error: "",
        s3Loading: false
      }
      break
    // case GET_IMAGE_S3:
    //   state = { ...state }
    //   break
    // case GET_IMAGE_S3_FAIL:
    //   state = { ...state, success: action.payload }
    //   break
    // case GET_IMAGE_S3_SUCCESS:
    //   state = { ...state, error: action.payload }
    //   break
    default:
      state = { ...state }
      break
  }
  // console.log("return state", state)
  return state
}

export default s3

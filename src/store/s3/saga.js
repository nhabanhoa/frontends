import { takeEvery, fork, put, all, call } from "redux-saga/effects"

// S3 Redux States
import { UPLOAD_IMAGE_S3 } from "_store/s3/actionTypes";
import { uploadImageS3Fail, uploadImageS3Success} from "./actions";

import {
  uploadFilesForProductS3
} from "_utils/storage/s3"

function* uploadImage({payload: {files}}) {
  // console.log("call upload image saga")
  try {
    const response = yield call(uploadFilesForProductS3, files)
    // console.log("upload image response", response)
    yield put(uploadImageS3Success(response))
  } catch (error) {
    yield put(uploadImageS3Fail(error))
  }
}

export function* watchS3() {
  yield takeEvery(UPLOAD_IMAGE_S3, uploadImage)
}

function* S3Saga() {
  yield all([fork(watchS3)])
}

export default S3Saga

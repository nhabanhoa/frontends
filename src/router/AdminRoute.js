import { checkAdmin } from '_utils/helpers'
import directRoute from './directRoute'
import { PAGE_TYPE } from '_constants'
const AdminRoute = directRoute(
  () => checkAdmin(),
  PAGE_TYPE.ADMIN, '/'
)
export default AdminRoute

import React, { Component, Suspense } from "react";
import AppRouter from '_router';
import "_utils/amplify/config"
import "react-table-v6/react-table.css"

import "_styles/theme.scss";
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

class App extends Component{
  render(){
    return(
      <React.Fragment>
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={true}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <AppRouter/>
      </React.Fragment>
    );
  }
}

export default App;

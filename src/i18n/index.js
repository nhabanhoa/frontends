import i18n from "i18next"
import detector from "i18next-browser-languagedetector"
import { initReactI18next } from "react-i18next"

import translationEN from "./en.json"
import translationVI from "./vi.json"

const DEFAULT_LANGUAGE = "vi"

// the translations
const resources = {
  vi: {
    translation: translationVI,
  },
  en: {
    translation: translationEN,
  }
}

const language = localStorage.getItem("I18N_LANGUAGE")
if (!language) {
  localStorage.setItem("I18N_LANGUAGE", DEFAULT_LANGUAGE)
}

i18n
  .use(detector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: localStorage.getItem("I18N_LANGUAGE") || DEFAULT_LANGUAGE,
    fallbackLng: DEFAULT_LANGUAGE, // use en if detected lng is not available

    keySeparator: '.', // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  })

export default i18n
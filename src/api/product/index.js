import makeRequest from ".."
import * as url from "../urls"

const searchProducts = body =>
  makeRequest.post(url.PRODUCT_SEARCH, body)

const getProductDetail = productId =>
  makeRequest.get(`${url.PRODUCT}/${productId}`, null)

const createProduct = info =>
  makeRequest.post(url.PRODUCT, info)

const deleteProduct = productId =>
  makeRequest.delete(url.PRODUCT+"/"+productId, null)

const updateProduct = (productId, info) =>
  makeRequest.put(url.PRODUCT+"/"+productId, info)

export {
  searchProducts,
  getProductDetail,
  createProduct,
  deleteProduct,
  updateProduct
}
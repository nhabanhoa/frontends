import axios from 'axios'
import Qs from 'qs'
import { API_METHOD } from '_constants';
// import {getAccessToken} from '../cookie-store'
const makeRequest = async (
  method,
  url,
  data
) => {
  let headers = {}
  headers["Content-Type"] = "application/json;charset=UTF-8";
  // headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS, PUT, DELETE";
  // headers["Access-Control-Allow-Origin"] = "*";
  // const token = getAccessToken();

  // if(token){
  //   headers['Authorization'] = `Bearer ${token}`
  // }

  axios.defaults.baseURL = process.env.REACT_APP_API

  console.log('url',axios.defaults.baseURL)

  const config = {
    url: url,
    headers: headers,
    method: method,
    paramsSerializer: function(params) {
      return Qs.stringify(params, {
        arrayFormat: "brackets"
      });
    }
  };

  if(method===API_METHOD.METHOD_GET){
    config.params = data;
  }else{
    config.data = data;
  }
  try{
    const res = await axios(config);
    if(res)
      return res;
    else
      return false;
  }catch(err){
    return err;
  }

}

export default {
  get(url, data){
    return makeRequest(API_METHOD.METHOD_GET, url, data);
  },

  post(url, data){
    return makeRequest(API_METHOD.METHOD_POST, url, data);
  },

  put(url, data){
    return makeRequest(API_METHOD.METHOD_PUT, url, data);
  },

  delete(url){
    return makeRequest(API_METHOD.METHOD_DELETE, url, null);
  },
}
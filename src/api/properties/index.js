import makeRequest from ".."
import * as url from "../urls"

const createColor = info =>
  makeRequest.post(url.PROPERTY+url.COLOR, info)

const createOrigin = info =>
  makeRequest.post(url.PROPERTY+url.ORIGIN, info)

const createPacking = info =>
  makeRequest.post(url.PROPERTY+url.PACKING, info)

const createSellBlock = info =>
  makeRequest.post(url.PROPERTY+url.SELLBLOCK, info)

const createType = info =>
  makeRequest.post(url.PROPERTY+url.TYPE, info)

const updateColor = (id, info) =>
  makeRequest.put(`${url.PROPERTY}${url.COLOR}/${id}`, info)

const updateOrigin = (id, info) =>
  makeRequest.put(`${url.PROPERTY}${url.ORIGIN}/${id}`, info)

const updatePacking = (id, info) =>
  makeRequest.put(`${url.PROPERTY}${url.PACKING}/${id}`, info)

const updateSellBlock = (id, info) =>
  makeRequest.put(`${url.PROPERTY}${url.SELLBLOCK}/${id}`, info)

const updateType = (id, info) =>
  makeRequest.put(`${url.PROPERTY}${url.TYPE}/${id}`, info)

const deleteColor = (id) =>
  makeRequest.delete(`${url.PROPERTY}${url.COLOR}/${id}`)

const deleteOrigin = (id) =>
  makeRequest.delete(`${url.PROPERTY}${url.ORIGIN}/${id}`)

const deletePacking = (id) =>
  makeRequest.delete(`${url.PROPERTY}${url.PACKING}/${id}`)

const deleteSellBlock = (id) =>
  makeRequest.delete(`${url.PROPERTY}${url.SELLBLOCK}/${id}`)

const deleteType = (id) =>
  makeRequest.delete(`${url.PROPERTY}${url.TYPE}/${id}`)

const getColors = () =>
  makeRequest.get(`${url.PROPERTY}${url.COLOR}`, null)

const getOrigins = () =>
  makeRequest.get(`${url.PROPERTY}${url.ORIGIN}`, null)

const getPackings = () =>
  makeRequest.get(`${url.PROPERTY}${url.PACKING}`, null)

const getSellBlocks = (id) =>
  makeRequest.get(`${url.PROPERTY}${url.SELLBLOCK}`, null)

const getTypes = (id) =>
  makeRequest.get(`${url.PROPERTY}${url.TYPE}`, null)

export {
  createColor,
  createOrigin,
  createPacking,
  createSellBlock,
  createType,
  updateColor,
  updateOrigin,
  updatePacking,
  updateSellBlock,
  updateType,
  getColors,
  getOrigins,
  getPackings,
  getSellBlocks,
  getTypes,
  deleteColor,
  deleteOrigin,
  deletePacking,
  deleteSellBlock,
  deleteType
}

import makeRequest from ".."
import * as url from "../urls"

const postForgetPwd = data => makeRequest.post(url.FORGET_PASSWORD, data)
const postLogin = data => makeRequest.post(url.LOGIN, data)
const postRegister = data => makeRequest.post(url.REGISTER, data)
export {
  postForgetPwd,
  postLogin,
  postRegister
}
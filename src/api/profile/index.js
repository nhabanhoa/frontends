import makeRequest from ".."
import * as url from "../urls"

const putProfile = (data) => makeRequest.put(url.PROFILE_ID.replace("{id}", data.id), data)

export {
  putProfile
}
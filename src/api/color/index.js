import makeRequest from '..'
import * as url from "../urls"

const getColors = params =>
  makeRequest.get(url.COLORS, params)

const addColors = params =>
  makeRequest.post(url.COLORS, params)

const editColors = params =>
  makeRequest.put(url.COLORS, params)

const deleteColors = params =>
  makeRequest.put(url.COLORS, params)

export {
  getColors,
  addColors,
  editColors,
  deleteColors,
}
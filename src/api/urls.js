//Auth
export const FORGET_PASSWORD = "/forget-password"
export const LOGIN = "/login"
export const REGISTER = "/register"

//Profile
export const PROFILE="/profile"
export const PROFILE_ID="/profile/{id}"

//Product
export const PRODUCT="/product"
export const PRODUCT_SEARCH="/product/search"

//Property
export const PROPERTY = "/property"
export const COLOR="/color"
export const ORIGIN = "/origin"
export const PACKING = "/packing"
export const SELLBLOCK = "/sellblock"
export const TYPE = "/type"

